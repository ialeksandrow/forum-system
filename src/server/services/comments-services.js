import serviceErrors from './service-errors.js';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {string} message
 * @return {object}
 */
const formatError = (message) => {
  return {
    errorMessage: message,
    data: null,
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} data
 * @return {object}
 */
const formatSuccess = (data) => {
  return {
    errorMessage: null,
    data: data,
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} commentsData
 * @return {object}
 */
const updateComment = (commentsData) => {
  return async (commentId, newCommentContent) => {
    const originalComment = await commentsData.getById(commentId);

    if (!originalComment) {
      return formatError(serviceErrors.RECORD_NOT_FOUND);
    }

    if (newCommentContent === originalComment.comment_content) {
      return formatError(serviceErrors.NO_MODIFICATION);
    }

    const commentHistoryParsed = JSON.parse(
        originalComment.comment_history);

    commentHistoryParsed.unshift({
      update_date: originalComment.comment_date,
      updated_comment: originalComment.comment_content,
    });

    const result = await commentsData
        .updateComment(commentId,
            newCommentContent,
            JSON.stringify(commentHistoryParsed));

    if ((await typeof result) === 'string') {
      return formatError(result);
    }

    return formatSuccess({
      result,
      originalComment: originalComment.comment_content,
      commentHistory: commentHistoryParsed,
      commentContent: newCommentContent,
    });
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} commentsData
 * @return {object}
 */
const deleteComment = (commentsData) => {
  return async (commentId) => {
    const result = await commentsData.deleteComment(commentId);

    if ((await typeof result) === 'string') {
      return formatError(result);
    }

    return formatSuccess(result);
  };
};

export {
  updateComment,
  deleteComment,
};
