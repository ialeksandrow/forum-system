import serviceErrors from './service-errors.js';
import userRoles from '../common/user-roles.enum.js';
import bcrypt from 'bcrypt';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function}
 */

const getUser = (userData) => async (id) => {
  if (id) {
    return userData.getUserById(id);
  } else {
    return userData.getAllUsers();
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|null}
 */

const getUserFriends = (userData) => async (id) => {
  if (id) {
    return userData.getUserFriends(id);
  } else {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|null}
 */

const getUserPosts = (userData) => async (id) => {
  if (id) {
    return userData.getUserPosts(id);
  } else {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|null}
 */

const getUserComments = (userData) => async (id) => {
  if (id) {
    return userData.getUserComments(id);
  } else {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|null}
 */

const getUserByUsername = (userData) => async (username) => {
  if (username) {
    return userData.getUserByUsername(username);
  } else {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|number}
 */

const createUser =
  (userData) =>
    async ({username, password, email, birthdate}) => {
      const existingUser = await userData.getUserByUsername(username);

      if (existingUser[0]) {
        return serviceErrors.DUPLICATE_RECORD;
      }

      const passwordHash = await bcrypt.hash(password, 10);
      const createdUser = await userData.createUser(
          username,
          passwordHash,
          email,
          birthdate,
          userRoles.USER,
      );

      if (createdUser) {
        return createdUser;
      } else {
      // database will throw for email as well (unique value required)
        return serviceErrors.DUPLICATE_RECORD;
      }
    };

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|number}
 */

const updateUser = (userData) => async (obj, id) => {
  const existingUser = await userData.getUserById(id);

  if (existingUser) {
    const user = {...existingUser};
    const userWithUpdates = {...user, ...obj};

    // when there are no user changes, skip update
    if (JSON.stringify(user) === JSON.stringify(userWithUpdates)) {
      return existingUser;
    } else {
      if (obj.password) {
        const passwordHash = await bcrypt.hash(obj.password, 10);
        obj.password = passwordHash;
      }

      return userData.updateUser(obj, id);
    }
  } else {
    return serviceErrors.RECORD_NOT_FOUND;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|number}
 */

const deleteUser = (userData) => async (id) => {
  const existingUser = await userData.getUserById(id);

  if (existingUser) {
    return userData.deleteUser(id);
  } else {
    return serviceErrors.RECORD_NOT_FOUND;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function|number}
 */

const banUser = (userData) => async (expiration, id) => {
  const existingUser = await userData.getUserById(id);

  if (existingUser) {
    return userData.banUser(expiration, id);
  } else {
    return serviceErrors.RECORD_NOT_FOUND;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function}
 */

const getBannedUsers = (userData) => async () => {
  return userData.getBannedUsers();
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {object|number}
 */

const signInUser = (userData) => async (username, password) => {
  const user = await userData.getUserData(username);

  if (!user) {
    return serviceErrors.RECORD_NOT_FOUND;
  }

  if (user.is_banned) {
    if (Date.now() < Date.parse(user.ban_expiration)) {
      return {
        err: serviceErrors.USER_BANNED,
        expiration: user.ban_expiration,
      };
    } else {
      await userData.removeUserBan(username);
    }
  }

  if (await bcrypt.compare(password, user.password)) {
    delete user.password;

    return user;
  } else {
    return serviceErrors.INVALID_SIGNIN;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function}
 */

const getUserStats = (userData) => async (id) => {
  const user = await userData.getUserById(id);

  if (!user) {
    return serviceErrors.RECORD_NOT_FOUND;
  }

  return userData.getUserStats(id);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function}
 */

const addFriend = (userData) => async (id, friend_id) => {
  const user = await userData.getUserById(id);
  const friend = await userData.getUserById(friend_id);

  if (!user || !friend) {
    return serviceErrors.RECORD_NOT_FOUND;
  }

  return userData.addFriend(id, friend_id);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @returns {function}
 */

const removeFriend = (userData) => async (id, friend_id) => {
  const user = await userData.getUserById(id);
  const friend = await userData.getUserById(friend_id);

  if (!user || !friend) {
    return serviceErrors.RECORD_NOT_FOUND;
  }

  return userData.removeFriend(id, friend_id);
};

export default {
  getUser,
  getUserFriends,
  getUserPosts,
  getUserComments,
  getUserByUsername,
  createUser,
  updateUser,
  deleteUser,
  banUser,
  getBannedUsers,
  signInUser,
  getUserStats,
  addFriend,
  removeFriend,
};
