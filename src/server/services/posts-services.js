import DEFAULT from '../common/default.enum.js';
import serviceErrors from './service-errors.js';

const formatError = (message) => {
  return {
    errorMessage: message,
    data: null,
  };
};

const formatSuccess = (data) => {
  return {
    errorMessage: null,
    data: data,
  };
};

const resultFormatter = async (result) => {
  if ((await typeof result) === 'string') {
    return {
      errorMessage: result,
      data: null,
    };
  }

  return {
    errorMessage: null,
    data: result,
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {object} postsData
 * @return {function}
 */
export const getPosts = (postsData) => {
  return async (filter) => {
    const count = +filter.count || DEFAULT.POST_COUNT;
    const offset = (filter.page - 1) * count || DEFAULT.POST_OFFSET;

    let result = [];

    if (
      Object.prototype.hasOwnProperty.call(filter, 'column') ||
      Object.prototype.hasOwnProperty.call(filter, 'value')
    ) {
      result = await postsData.searchBy(
          filter.column,
          filter.value,
          offset,
          count,
      );
    } else {
      result = await postsData.getAll(offset, count);
    }
    const pagesCount = await postsData.getNumberPages(count);

    return await resultFormatter({
      result,
      pagesCount: pagesCount[0].pages_count,
    });
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} postsData
 * @param {object} postLikesData
 * @return {object}
 */
export const getPostById = (postsData, postLikesData) => {
  return async (userId, postId) => {
    const result = await postsData.getById(postId);
    const isLiked = userId && (await postLikesData.getOne(userId, postId));

    if (userId) {
      result.isLiked = !!isLiked[0];
    }

    if (result?.post_history) {
      result.post_history = JSON.parse(result.post_history);
    }
    return await resultFormatter(result);
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {object} postsData
 * @param {pbject} logsData
 * @return {function}
 */
export const createPost = (postsData) => {
  return async (userId, postTitle, postContent) => {
    const result = await postsData.createPost(userId, postTitle, postContent);

    return await resultFormatter(result);
  };
};

export const updatePost = (postsData) => {
  return async (postId, newPostContent) => {
    const originalPost = await postsData.getById(postId);

    if (newPostContent === originalPost.post_content) {
      return formatError(serviceErrors.NO_MODIFICATION);
    }

    const postHistoryParsed = JSON.parse(originalPost.post_history);
    postHistoryParsed.unshift({
      update_date: originalPost.post_date,
      updated_post: originalPost.post_content,
    });
    const result = await postsData.updatePost(
        postId,
        newPostContent,
        JSON.stringify(postHistoryParsed),
    );

    if ((await typeof result) === 'string') {
      return formatError(result);
    }

    return formatSuccess({
      result,
      originalPost: originalPost.post_content,
      postHistory: postHistoryParsed,
      postContent: newPostContent,
    });
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} postsData
 * @return {object}
 */
export const deletePost = (postsData) => {
  return async (postId) => {
    const originalPost = await postsData.getById(postId);

    if (!originalPost) {
      return formatError(serviceErrors.RECORD_NOT_FOUND);
    }

    if (originalPost.is_deleted == 1) {
      return formatError(serviceErrors.NO_MODIFICATION);
    }

    const result = await postsData.deletePost(postId);

    return formatSuccess(result);
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} postsData
 * @return {object}
 */
export const getPostLikes = (postsData) => {
  return async (postId) => {
    const result = await postsData.getPostLikes(postId);

    return await resultFormatter(result);
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} postsData
 * @param {object} commentLikesData
 * @return {object}
 */
export const getPostComments = (postsData, commentLikesData) => {
  return async (userId, postId, filter) => {
    // If page or count from URL query are NaN, evaluated to undefined.
    // postsData.getPostComments has default values for them.
    const offset =
      (filter.page - 1) * (filter.count || DEFAULT.COMMENT_COUNT) || undefined;
    const count = +filter.count || undefined;
    const result = await postsData.getPostComments(postId, offset, count);

    const resultWithLikes = Promise.all(
        result.map(async (comment) => {
          if (userId) {
            const isLiked = await commentLikesData.getOne(
                userId,
                comment.comment_id,
            );

            comment.isLiked = !!isLiked[0];
          }
          return comment;
        }),
    );

    const parsedResult = ((await resultWithLikes) || result).map((comment) =>
      Object.assign(comment, {
        comment_history: JSON.parse(comment.comment_history),
      }),
    );

    return await resultFormatter(parsedResult);
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} postsData
 * @return {object}
 */
export const createPostComment = (postsData) => {
  return async (userId, postId, commentContent) => {
    const result = await postsData.createPostComment(
        userId,
        postId,
        commentContent,
    );

    return await resultFormatter(result);
  };
};
