/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} likesData
 * @return {object}
 */
const addLike = (likesData) => {
  return async (userId, contentId) => {
    const result = await likesData.createLike(userId, contentId);

    if (await typeof result === 'string') {
      return {
        errorMessage: result,
        data: null,
      };
    }

    return {
      errorMessage: null,
      data: result,
    };
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} likesData
 * @return {object}
 */
const removeLike = (likesData) => {
  return async (userId, contentId) => {
    const result = await likesData.deleteLike(userId, contentId);

    if (await typeof result === 'string') {
      return {
        errorMessage: result,
        data: null,
      };
    }

    return {
      errorMessage: null,
      data: result,
    };
  };
};

export {
  addLike,
  removeLike,
};
