import * as postsServices from '../services/posts-services.js';
import * as commentsServices from '../services/comments-services.js';
import * as likesServices from '../services/likes-services.js';
import * as postsData from '../data/posts-data.js';
import * as commentsData from '../data/comments-data.js';
import * as postLikesData from '../data/post-likes-data.js';
import * as commentLikesData from '../data/comment-likes-data.js';
import serviceErrors from '../services/service-errors.js';
import statusCodes from '../common/status-codes.enum.js';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerik.com>
 *
 * @param {object} result
 * @param {object} res
 * @return {boolean}
 */
const sendError = (result, res) => {
  if (typeof result.errorMessage === 'string') {
    res.status(400).json({
      errorMessage: result.errorMessage,
      data: null,
    });
    return true;
  }

  if (result.errorMessage === serviceErrors.OPERATION_NOT_PERMITTED) {
    res.status(401).json({
      errorMessage: 'Operation not permitted.',
      data: null,
    });

    return true;
  }

  if (result.errorMessage === serviceErrors.NO_MODIFICATION) {
    res.status(400).json({
      errorMessage: 'No modifications recorded.',
      data: null,
    });

    return true;
  }

  if (result.errorMessage === serviceErrors.RECORD_NOT_FOUND) {
    res.status(404).json({
      errorMessage: 'Content not found',
      data: null,
    });

    return true;
  }

  if (!result.data) {
    res.status(400).json({
      errorMessage: 'No results',
      data: null,
    });
    return true;
  }

  return false;
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerik.com>
 *
 * @param {number} successCode
 * @param {object} result
 * @param {object} res
 * @return {boolean}
 */
const sendResult = async (successCode, result, res) => {
  if (typeof result === 'object') {
    const returnObject = Object.assign(
        {status: statusCodes[successCode]},
        result,
    );
    return res.status(successCode).json(returnObject);
  }
  return res.status(successCode).json({
    status: statusCodes[successCode],
    data: result,
  });
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {object} req
 * @param {object} res
 */
const getPosts = async (req, res) => {
  const filter = req.query;
  const posts = await postsServices.getPosts(postsData)(filter);

  return sendError(posts, res) || sendResult(200, posts, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerik.com>
 *
 * @param {object} req
 * @param {object} res
 * @return {boolean}
 */
const getPostById = async (req, res) => {
  const userId = +req.user.user_id;
  const postId = +req.params.postId;
  const filter = req.query;
  const post = await postsServices.getPostById(postsData, postLikesData)(userId, postId);
  if (sendError(post, res)) return;

  const comments = await postsServices.getPostComments(postsData, commentLikesData)(
      userId,
      postId,
      filter,
  );

  return sendResult(200, {post: post, comments: comments}, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const createPost = async (req, res) => {
  const result = await postsServices.createPost(postsData)(
      +req.user.user_id,
      req.body.post_title,
      req.body.post_content,
  );

  if (sendError(result, res)) return;

  return sendError(result, res) || sendResult(201, result.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const updatePost = async (req, res) => {
  const result = await postsServices.updatePost(postsData)(
      +req.params.postId,
      req.body.post_content,
  );

  return sendError(result, res) || sendResult(200, result.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const deletePost = async (req, res) => {
  const result = await postsServices.deletePost(postsData)(
      +req.params.postId,
  );

  return sendError(result, res) || sendResult(200, result.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {object} likesData
 * @param {string} contentId
 * @return {function}
 */
const addLike = (likesData, contentId) => {
  return async (req, res) => {
    const result = await likesServices.addLike(likesData)(
        +req.user.user_id,
        +contentId,
    );

    return sendError(result, res) || sendResult(201, result.data, res);
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {object} likesData
 * @param {string} contentId
 * @return {function}
 */
const removeLike = (likesData, contentId) => {
  return async (req, res) => {
    const result = await likesServices.removeLike(likesData)(
        +req.user.user_id,
        +contentId,
    );

    return sendError(result, res) || sendResult(200, result.data, res);
  };
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const addPostLike = async (req, res) => {
  return addLike(postLikesData, +req.params.postId)(req, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const removePostLike = async (req, res) => {
  return removeLike(postLikesData, +req.params.postId)(req, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const getPostLikes = async (req, res) => {
  const results = await postsServices.getPostLikes(postsData)(
      +req.params.postId,
  );

  return sendError(results, res) || sendResult(200, results.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const createPostComment = async (req, res) => {
  const result = await postsServices.createPostComment(postsData)(
      +req.user.user_id,
      +req.params.postId,
      req.body.comment_content,
  );

  return sendError(result, res) || sendResult(201, result.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const updatePostComment = async (req, res) => {
  const result = await commentsServices.updateComment(commentsData)(
      +req.params.commentId,
      req.body.comment_content,
  );
  console.log(result);
  return sendError(result, res) || sendResult(200, result.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const deletePostComment = async (req, res) => {
  const result = await commentsServices.deleteComment(commentsData)(
      +req.params.commentId);

  return sendError(result, res) || sendResult(200, result.data, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const addCommentLike = async (req, res) => {
  return addLike(commentLikesData, +req.params.commentId)(req, res);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {*} req
 * @param {*} res
 * @return {boolean}
 */
const removeCommentLike = async (req, res) => {
  return removeLike(commentLikesData, +req.params.commentId)(req, res);
};

export {
  getPosts,
  getPostById,
  createPost,
  updatePost,
  deletePost,
  getPostLikes,
  addPostLike,
  removePostLike,
  createPostComment,
  updatePostComment,
  deletePostComment,
  addCommentLike,
  removeCommentLike,
};
