import usersData from '../data/users-data.js';
import usersService from '../services/users-service.js';
import serviceErrors from '../services/service-errors.js';
import createToken from '../authentication/create-token.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const authenticateUser = async (req, res) => {
  const {username, password} = req.body;
  const user = await usersService.signInUser(usersData)(username, password);

  if (user === serviceErrors.RECORD_NOT_FOUND || user.is_deactivated !== 0) {
    return res
        .status(404)
        .json({msg: `There is no record of user with username ${username}.`});
  } else if (
    user.err ||
    (user.expiration && user.err === serviceErrors.USER_BANNED)
  ) {
    return res.status(403).json({
      msg: `There is an active ban on user with username ${username}. It will expire on ${user.expiration.toLocaleString()}.`,
    });
  } else if (user === serviceErrors.INVALID_SIGNIN) {
    return res
        .status(400)
        .json({msg: `Incorrect account password for ${username}.`});
  } else {
    const payload = {
      user_id: user.user_id,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      birthdate: user.birthdate,
      registration_date: user.registration_date,
      role: user.role_type,
    };

    const token = createToken(payload);

    return res.status(200).json({
      status: '200 - OK',
      token: token,
    });
  }
};

export default authenticateUser;
