import usersData from '../data/users-data.js';
import usersService from '../services/users-service.js';
import serviceErrors from '../services/service-errors.js';
import banDurations from '../common/ban-durations.enum.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const getUsers = async (req, res) => {
  const {q} = req.query;
  const {id} = req.params;
  let usersList;

  if (q) {
    usersList = await usersService.getUserByUsername(usersData)(q);

    if (!!q && !usersList[0]) {
      return res
          .status(404)
          .json({msg: `There is no record of user with username ${q}.`});
    }
  } else {
    usersList = await usersService.getUser(usersData)(id);

    if (!!id && !usersList) {
      return res
          .status(404)
          .json({msg: `There is no record of user with id ${id}.`});
    }
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      users: usersList.length,
      results: usersList,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const getUserFriends = async (req, res) => {
  const {id} = req.params;
  const friendList = await usersService.getUserFriends(usersData)(id);

  if (!friendList) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      users: friendList.length,
      results: friendList,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const getUserPosts = async (req, res) => {
  const {id} = req.params;
  const postsList = await usersService.getUserPosts(usersData)(id);

  if (!postsList) {
    return res
        .status(404)
        .json({msg: `There is no record of post with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      posts: postsList.length,
      results: postsList,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const getUserComments = async (req, res) => {
  const {id} = req.params;
  const commentsList = await usersService.getUserComments(usersData)(id);

  if (!commentsList) {
    return res
        .status(404)
        .json({msg: `There is no record of comment with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      comments: commentsList.length,
      results: commentsList,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const createUser = async (req, res) => {
  const givenUser = req.body;
  const createdUser = await usersService.createUser(usersData)(givenUser);

  if (createdUser === serviceErrors.DUPLICATE_RECORD) {
    return res.status(409).json({
      msg: `Either ${givenUser.username} is already taken as username, or ${givenUser.email} is already attached to another account's email.`,
    });
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      ...createdUser[0],
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const updateUser = async (req, res) => {
  const givenUpdate = req.body;
  const {id} = req.params;
  const updatedUser = await usersService.updateUser(usersData)(givenUpdate, id);

  if (updatedUser === serviceErrors.RECORD_NOT_FOUND) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      ...updatedUser,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const deleteUser = async (req, res) => {
  const {id} = req.params;
  const deletedUser = await usersService.deleteUser(usersData)(id);

  if (deletedUser === serviceErrors.RECORD_NOT_FOUND) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      ...deletedUser[0],
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const banUser = async (req, res) => {
  const {duration} = req.body;
  const {id} = req.params;

  const banDuration = new Date(
      Date.now() + banDurations[duration.toUpperCase()],
  );
  const bannedUser = await usersService.banUser(usersData)(banDuration, id);

  if (bannedUser === serviceErrors.RECORD_NOT_FOUND) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      results: bannedUser,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const getBannedUsers = async (_, res) => {
  const usersList = await usersService.getBannedUsers(usersData)();

  return res.status(200).json({
    status: '200 - OK',
    data: {
      users: usersList.length,
      results: usersList,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const getUserStats = async (req, res) => {
  const {id} = req.params;
  const userStats = await usersService.getUserStats(usersData)(id);

  if (userStats === serviceErrors.RECORD_NOT_FOUND) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      ...userStats,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const addFriend = async (req, res) => {
  const {id} = req.params;
  const {friend_id} = req.body;
  const friendsList = await usersService.addFriend(usersData)(id, friend_id);

  if (friendsList === serviceErrors.RECORD_NOT_FOUND) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res.status(200).json({
    status: '200 - OK',
    data: {
      results: friendsList,
    },
  });
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 */

const removeFriend = async (req, res) => {
  const {id} = req.params;
  const {friend_id} = req.body;
  const removedUser = await usersService.removeFriend(usersData)(id, friend_id);

  if (removedUser === serviceErrors.RECORD_NOT_FOUND) {
    return res
        .status(404)
        .json({msg: `There is no record of user with id ${id}.`});
  }

  return res
      .status(200)
      .json({msg: `User with id ${friend_id} was removed from friends.`});
};

export default {
  getUsers,
  getUserFriends,
  getUserPosts,
  getUserComments,
  createUser,
  updateUser,
  deleteUser,
  banUser,
  getBannedUsers,
  getUserStats,
  addFriend,
  removeFriend,
};
