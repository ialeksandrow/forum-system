import './config.js';
import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import apiVersion from './middleware/api-version.js';
import postsRouter from './routers/posts-router.js';
import {usersRouter} from './routers/users-router.js';
import {authRouter} from './routers/auth-router.js';
import passport from 'passport';
import jwtStrategy from './authentication/strategy.js';
import {authMiddleware, roleMiddleware} from './middleware/auth.js';

export const app = express();
const PORT = process.env.EXPRESS_PORT;

passport.use(jwtStrategy);

// third-party middlewares
app.use(express.json(), cors(), helmet(), morgan('dev'), passport.initialize());

// self-written middlewares
app.use(apiVersion);
app.use('/api/sign-in', authRouter);
app.use('/api/posts', authMiddleware, roleMiddleware(['user', 'admin']), postsRouter);
app.use('/api/users', usersRouter);

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
