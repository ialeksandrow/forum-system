import pool from './pool.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {array}
 */

const getAllUsers = async () => {
  const sql = `
  SELECT user_id, username, email, first_name, last_name, birthdate, registration_date
  FROM users
  WHERE is_deactivated = 0;
  `;

  return await pool.query(sql);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @returns {object}
 */

const getUserById = async (id) => {
  const sql = `
  SELECT u.user_id, u.username, u.email, u.first_name, u.last_name, u.birthdate, u.registration_date, r.role_type AS role
  FROM users AS u
  JOIN roles AS r
  ON u.role_id = r.role_id
  WHERE is_deactivated = 0 AND user_id = ?;
  `;

  const user = await pool.query(sql, [+id]);

  return user[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @returns {array}
 */

const getUserFriends = async (id) => {
  const sql = `
  SELECT u.user_id, u.username, u.email, u.first_name, u.last_name, u.birthdate, u.registration_date
  FROM users AS u
  JOIN friends AS f
  ON u.user_id = f.user_2_id
  WHERE f.user_1_id = ?;
  `;

  return await pool.query(sql, [+id]);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @returns {array}
 */

const getUserPosts = async (id) => {
  const sql = `
  SELECT p.post_id, p.post_title, p.post_content, p.post_date, p.post_views
  FROM users AS u
  JOIN posts AS p
  ON u.user_id = p.user_id
  WHERE u.is_deactivated = 0 AND p.user_id = ?;
  `;

  return await pool.query(sql, [+id]);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @returns {array}
 */

const getUserComments = async (id) => {
  const sql = `
  SELECT p.post_id, p.post_title, c.comment_id, c.comment_content, c.comment_date
  FROM users AS u
  JOIN comments AS c
  ON u.user_id = c.user_id
  JOIN posts AS p
  ON p.post_id = c.post_id
  WHERE u.is_deactivated = 0 AND c.user_id = ?;
  `;

  return await pool.query(sql, [+id]);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @returns {array}
 */

const getUserByUsername = async (username) => {
  const sql = `
  SELECT user_id, username, email, first_name, last_name, birthdate, registration_date
  FROM users
  WHERE is_deactivated = 0 AND username = ?;
  `;

  return await pool.query(sql, [username]);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @param {string}
 * @param {string}
 * @param {string}
 * @param {string}
 * @returns {array|null}
 */

const createUser = async (username, password, email, birthdate, roleId) => {
  try {
    const sql = `
  INSERT INTO users(username, password, email, birthdate, role_id)
  VALUES (?, ?, ?, ?, ?);
  `;

    const createdUser = await pool.query(sql, [
      username,
      password,
      email,
      birthdate,
      roleId,
    ]);

    const sqlCreated = `
  SELECT user_id, username, email, first_name, last_name, birthdate, registration_date
  FROM users
  WHERE user_id = ?;
  `;

    const selectCreatedUser = await pool.query(sqlCreated, [
      createdUser.insertId,
    ]);

    return selectCreatedUser;
  } catch (err) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object}
 * @param {number}
 * @returns {array}
 */

const updateUser = async (obj, id) => {
  const sqlArgs = Object.keys(obj)
      .map((key, index) => `${key} = '${Object.values(obj)[index]}'`)
      .join(', ');

  const sql = `
  UPDATE users
  SET ${sqlArgs}
  WHERE user_id = ?;
  `;

  await pool.query(sql, [+id]);

  return getUserById(+id);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @returns {array}
 */

const deleteUser = async (id) => {
  const deletedUser = getUserById(+id);

  const sql = `
  UPDATE users
  SET is_deactivated = 1
  WHERE user_id = ?;
  `;

  await pool.query(sql, [+id]);

  return deletedUser;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @param {number}
 * @returns {array}
 */

const banUser = async (expiration, id) => {
  const sql = `
  UPDATE users
  SET is_banned = 1, ban_date = NOW(), ban_expiration = ?
  WHERE user_id = ?;
  `;

  await pool.query(sql, [expiration, +id]);

  const sqlBanned = `
  SELECT user_id, username, email, first_name, last_name, birthdate, registration_date, ban_date, ban_expiration
  FROM users
  WHERE user_id = ?;
  `;

  const selectBannedUser = await pool.query(sqlBanned, [+id]);

  return selectBannedUser;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @returns {boolean}
 */

const removeUserBan = async (username) => {
  try {
    const sql = `
  UPDATE users
  SET is_banned = 0, ban_expiration = '0000-00-00 00:00:00'
  WHERE username = ?;
  `;

    await pool.query(sql, [username]);

    return true;
  } catch (err) {
    return false;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {array}
 */

const getBannedUsers = async () => {
  const sql = `
  SELECT user_id, username, email, first_name, last_name, birthdate, registration_date, ban_date, ban_expiration
  FROM users
  WHERE is_deactivated = 0 AND is_banned = 1;
  `;

  return await pool.query(sql);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @returns {object}
 */

const getUserData = async (username) => {
  const sql = `
  SELECT u.user_id, u.username, u.password, u.first_name, u.last_name, u.birthdate, u.email, u.registration_date, u.ban_expiration, u.is_deactivated, u.is_banned, r.role_type
  FROM users AS u
  JOIN roles AS r
  ON u.role_id = r.role_id
  WHERE u.username = ?;
  `;

  const user = await pool.query(sql, [username]);

  return user[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @returns {array}
 */

const getUserStats = async (id) => {
  try {
    const sql = `
  SELECT (SELECT COUNT(p.user_id) FROM posts AS p WHERE p.user_id = u.user_id) AS posts_count,
         (SELECT COUNT(c.user_id) FROM comments AS c WHERE c.user_id = u.user_id) AS comments_count,
         (SELECT COUNT(f.user_2_id) FROM friends AS f WHERE f.user_1_id = u.user_id) AS friends_count
  FROM users AS u
  WHERE u.user_id = ?
  GROUP BY u.user_id;
  `;

    const stats = await pool.query(sql, [+id]);

    return stats[0];
  } catch (err) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @param {number}
 * @returns {array}
 */

const addFriend = async (id, friend_id) => {
  const sql = `
  INSERT INTO friends(user_1_id, user_2_id)
  VALUES (?, ?);
  `;

  const sqlFriends = `
  SELECT user_2_id
  FROM friends
  WHERE user_1_id = ?;
  `;

  try {
    await pool.query(sql, [+id, +friend_id]);

    return await pool.query(sqlFriends, [+id]);
  } catch (err) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @param {number}
 * @returns {array}
 */

const removeFriend = async (id, friend_id) => {
  const sql = `
  DELETE FROM friends
  WHERE user_1_id = ? AND user_2_id = ?;
  `;

  try {
    return await pool.query(sql, [+id, +friend_id]);
  } catch (err) {
    return null;
  }
};

export default {
  getAllUsers,
  getUserById,
  getUserFriends,
  getUserPosts,
  getUserComments,
  getUserByUsername,
  createUser,
  updateUser,
  deleteUser,
  banUser,
  removeUserBan,
  getBannedUsers,
  getUserData,
  getUserStats,
  addFriend,
  removeFriend,
};
