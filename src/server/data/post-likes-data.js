import pool from './pool.js';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} postId
 * @return {array}
 */
const getAll = async (postId) => {
  const sql = `
        SELECT * FROM post_likes
        WHERE post_id = ?`;

  try {
    return await pool.query(sql, [postId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} postId
 * @return {array}
 */
const getOne = async (userId, postId) => {
  const sql = `
        SELECT * FROM post_likes
        WHERE user_id = ? AND post_id = ?`;

  try {
    return await pool.query(sql, [userId, postId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} postId
 * @return {array}
 */
const createLike = async (userId, postId) => {
  const sql = `
        INSERT INTO post_likes (user_id, post_id)
        VALUES (?, ?)`;

  try {
    return await pool.query(sql, [userId, postId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} postId
 * @return {array}
 */
const deleteLike = async (userId, postId) => {
  const sql = `
    DELETE FROM post_likes
    WHERE user_id = ? AND post_id = ?;
    `;

  try {
    const result = await pool.query(sql, [userId, postId]);

    if (await result.affectedRows === 0) {
      const error = Object.assign(new Error(), {text: 'Not liked'});
      throw (error);
    }

    return await result;
  } catch (err) {
    return err.text;
  }
};

export {
  getAll,
  getOne,
  createLike,
  deleteLike,
};
