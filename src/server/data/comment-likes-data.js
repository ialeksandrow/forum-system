import pool from './pool.js';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} commentId
 * @return {object}
 */
const getAll = async (commentId) => {
  const sql = `
        SELECT * FROM comment_likes
        WHERE comment_id = ?`;

  try {
    return await pool.query(sql, [commentId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} commentId
 * @return {object}
 */
const getOne = async (userId, commentId) => {
  const sql = `
        SELECT * FROM comment_likes
        WHERE user_id = ? AND comment_id = ?`;

  try {
    return await pool.query(sql, [userId, commentId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} commentId
 * @return {object}
 */
const createLike = async (userId, commentId) => {
  const sql = `
        INSERT INTO comment_likes (user_id, comment_id)
        VALUES (?, ?)`;

  try {
    return await pool.query(sql, [userId, commentId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} commentId
 * @return {object}
 */
const deleteLike = async (userId, commentId) => {
  const sql = `
    DELETE FROM comment_likes
    WHERE user_id = ? AND comment_id = ?;
    `;

  try {
    const result = await pool.query(sql, [userId, commentId]);

    if (await result.affectedRows === 0) {
      const error = Object.assign(new Error(), {text: 'Not liked'});
      throw (error);
    }

    return await result;
  } catch (err) {
    return err.text;
  }
};


export {
  getAll,
  getOne,
  createLike,
  deleteLike,
};
