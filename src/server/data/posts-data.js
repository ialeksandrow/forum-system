import DEFAULT from '../common/default.enum.js';
import pool from './pool.js';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} offset
 * @param {number} count
 * @return {array}
 */
const getAll = async (offset = DEFAULT.POST_OFFSET, count = DEFAULT.POST_COUNT) => {
  const sql = `
    SELECT
      p.post_id,
      p.post_title,
      p.post_date,
      p.user_id,
    CASE
      WHEN p.post_history != '[]' THEN 1
      ELSE 0
      END as is_edited,
    u1.username,
    c.post_comments_count,
      l.post_likes_count
    FROM POSTS p
    JOIN users u1
      on p.user_id = u1.user_id
    LEFT JOIN (
      SELECT post_id, COUNT(user_id) post_likes_count
        FROM post_likes
      GROUP BY post_id
        ) l
      ON p.post_id = l.post_id
    LEFT JOIN (
      SELECT post_id, COUNT(comment_id) post_comments_count
      FROM comments
      WHERE is_deleted = 0
        GROUP BY post_id
      ) c
      on p.post_id = c.post_id
    WHERE p.is_deleted = 0
    ORDER BY p.post_date DESC
    LIMIT ?, ?`;

  return await pool.query(sql, [offset, count]);
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} count
 * @return {array}
 */
const getNumberPages = async (count = DEFAULT.POST_COUNT) => {
  const sql = `
    SELECT CASE
      WHEN MOD(COUNT(post_id), ?) = 0 THEN COUNT(post_id) DIV ?
      ELSE COUNT(post_id) DIV ? + 1
      END
      as pages_count
    FROM posts
    WHERE is_deleted = 0`;

  try {
    return await pool.query(sql, [count, count, count]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} id
 * @return {array}
 */
const getById = async (id) => {
  const sql = `
    SELECT p.*, u.username, COUNT(pl.user_id) post_likes_count
    FROM posts p
    JOIN users u
      ON p.user_id = u.user_id
    LEFT JOIN post_likes pl
      ON pl.post_id = p.post_id
    WHERE p.post_id = ?`;

  try {
    const result = await pool.query(sql, [id]);
    return result[0];
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {string} column
 * @param {string} value
 * @param {number} offset
 * @param {number} count
 *
 * @return {array}
 */
const searchBy = async (column, value, offset, count) => {
  const sql = `
    SELECT *
    FROM posts
    WHERE ${column} LIKE '%${value}%'
    ORDER BY post_date DESC
    LIMIT ?, ?`;

  try {
    return await pool.query(sql, [offset, count]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @param {number} postTitle
 * @param {string} postContent
 *
 * @return {object | string}
 */
const createPost = async (userId, postTitle, postContent) => {
  const sql = `
    INSERT INTO posts (
      post_title,
      post_content,
      user_id)
    VALUES (?, ?, ?)`;
  try {
    return await pool.query(sql, [postTitle, postContent, userId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} postId
 * @param {string} newPostContent
 * @param {string} postHistory
 * @return {array}
 */
const updatePost = async (postId, newPostContent, postHistory) => {
  const sql = `
    UPDATE posts
    SET post_content = ?, post_history = ?, post_date = CURRENT_TIMESTAMP
    WHERE post_id = ?`;

  try {
    return await pool.query(sql, [newPostContent, postHistory, postId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} postId
 * @return {array}
 */
const deletePost = async (postId) => {
  const sql = `
    UPDATE posts
    SET is_deleted = 1
    WHERE post_id = ?`;

  try {
    return await pool.query(sql, [postId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} postId
 * @return {array}
 */
const getPostLikes = async (postId) => {
  const sql = `
    SELECT u.user_id, u.username
    FROM post_likes p
    JOIN users u
      on p.user_id = u.user_id
    WHERE p.post_id = ?`;

  try {
    return await pool.query(sql, [postId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} postId
 * @param {number} offset
 * @param {number} count
 * @return {array}
 */
const getPostComments = async (
    postId,
    offset = DEFAULT.COMMENT_OFFSET,
    count = DEFAULT.COMMENT_COUNT,
) => {
  const sql = `
    SELECT
      c.comment_id,
      c.comment_content,
      c.comment_date,
      u.username,
      c.comment_history,
      c.user_id,
      COUNT(cl.user_id) comment_likes_count
    FROM comments c
    JOIN posts p
      ON c.post_id = p.post_id
    JOIN users u
      ON c.user_id = u.user_id
    LEFT JOIN comment_likes cl
      ON cl.comment_id = c.comment_id
    WHERE p.post_id = ? AND c.is_deleted = 0
    GROUP BY c.comment_id
    ORDER BY c.comment_date DESC
    LIMIT ?, ?`;

  try {
    return await pool.query(sql, [postId, offset, count]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} userId
 * @param {number} postId
 * @param {string} commentContent
 * @return {array}
 */
const createPostComment = async (userId, postId, commentContent) => {
  const sql = `
    INSERT INTO comments
      (user_id, post_id, comment_content)
    VALUE
      (?, ?, ?)`;

  try {
    const result = await pool.query(sql, [userId, postId, commentContent]);
    return {...result, commentContent};
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} commentId
 * @return {array}
 */
const deletePostComment = async (commentId) => {
  const sql = `
    UPDATE comments
    SET is_deleted = 1
    WHERE comment_id = ?`;

  try {
    return await pool.query(sql, [commentId]);
  } catch (err) {
    return err.text;
  }
};

export {
  getAll,
  getById,
  searchBy,
  getNumberPages,
  createPost,
  updatePost,
  deletePost,
  getPostLikes,
  getPostComments,
  createPostComment,
  deletePostComment,
};
