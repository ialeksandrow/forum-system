import pool from './pool.js';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} value
 * @return {array}
 */
const getById = async (value) => {
  const sql = `
    SELECT *
    FROM comments
    where comment_id = ?`;

  try {
    const result = await pool.query(sql, [value]);
    return result[0];
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} commentId
 * @param {string} newCommentContent
 * @param {string} commentHistory
 * @return {array}
 */
const updateComment = async (commentId, newCommentContent, commentHistory) => {
  const sql = `
    UPDATE comments
    SET comment_content = ?, comment_history = ?, comment_date = CURRENT_TIMESTAMP
    WHERE comment_id = ?`;

  try {
    return await pool.query(sql, [
      newCommentContent,
      commentHistory,
      commentId]);
  } catch (err) {
    return err.text;
  }
};

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} userId
 * @return {array}
 */
const deleteComment = async (userId) => {
  const sql = `
    UPDATE comments
    SET is_deleted = 1
    WHERE comment_id = ?`;

  try {
    return await pool.query(sql, [userId]);
  } catch (err) {
    return err.text;
  }
};

export {
  getById,
  updateComment,
  deleteComment,
};
