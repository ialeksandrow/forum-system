import express from 'express';
import * as postsController from '../controllers/posts-controllers.js';
import idChecker from '../middleware/id-checker.js';
import validator from '../middleware/validator.js';
import * as postsSchemas from '../validators/posts.schemas.js';
import * as commentsSchemas from '../validators/comments.schemas.js';

const router = new express.Router();
export default router;

router.param('postId', idChecker);
router.param('commentId', idChecker);

router
    .route('/:postId/comments/:commentId/likes')
    .post(postsController.addCommentLike)
    .delete(postsController.removeCommentLike);

router
    .route('/:postId/comments/:commentId')
    .put(
        validator(commentsSchemas.createCommentSchema),
        postsController.updatePostComment,
    )
    .delete(postsController.deletePostComment);

router
    .route('/:postId/likes')
    .get(postsController.getPostLikes)
    .post(postsController.addPostLike)
    .delete(postsController.removePostLike);

router
    .route('/:postId')
    .get(postsController.getPostById)
    .post(
        validator(commentsSchemas.createCommentSchema),
        postsController.createPostComment,
    )
    .put(validator(postsSchemas.updatePostSchema), postsController.updatePost)
    .delete(postsController.deletePost);

router
    .route('/')
    .get(postsController.getPosts)
    .post(validator(postsSchemas.createPostSchema), postsController.createPost);
