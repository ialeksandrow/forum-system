import express from 'express';
import validator from '../middleware/validator.js';
import postValidator from '../middleware/post-validator.js';
import usersSchema from '../validators/users.schema.js';
import usersUpdateSchema from '../validators/users-update.schema.js';
import banSchema from '../validators/ban.schema.js';
import usersControllers from '../controllers/users-controllers.js';
import {authMiddleware, roleMiddleware} from '../middleware/auth.js';

export const usersRouter = new express.Router();

usersRouter
    .route('/')
    .get(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.getUsers,
    )
    .post(validator(usersSchema), usersControllers.createUser);

usersRouter
    .route('/banned')
    .get(
        authMiddleware,
        roleMiddleware('admin'),
        usersControllers.getBannedUsers,
    );

usersRouter
    .route('/:id')
    .get(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.getUsers,
    )
    .put(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        postValidator(usersUpdateSchema),
        usersControllers.updateUser,
    )
    .delete(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.deleteUser,
    );

usersRouter
    .route('/:id/friends')
    .get(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.getUserFriends,
    )
    .post(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.addFriend,
    )
    .delete(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.removeFriend,
    );

usersRouter
    .route('/:id/posts')
    .get(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.getUserPosts,
    );

usersRouter
    .route('/:id/comments')
    .get(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.getUserComments,
    );

usersRouter
    .route('/:id/ban')
    .put(
        authMiddleware,
        roleMiddleware('admin'),
        validator(banSchema),
        usersControllers.banUser,
    );

usersRouter
    .route('/:id/stats')
    .get(
        authMiddleware,
        roleMiddleware(['user', 'admin']),
        usersControllers.getUserStats,
    );
