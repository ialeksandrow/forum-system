export default {
  HOUR: 3600000,
  DAY: 86400000,
  WEEK: 604800000,
};
