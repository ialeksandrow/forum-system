export default {
  200: '200 - OK',
  201: '201 - Created',
  400: '400 - Bad Request',
  401: '401 - Not Authorised',
  404: '404 - Not Found',
};
