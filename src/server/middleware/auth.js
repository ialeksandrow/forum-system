import passport from 'passport';

export const authMiddleware = passport.authenticate('jwt', {session: false});

export const roleMiddleware = (role) => {
  return (req, res, next) => {
    if (
      (req.user && req.user.role === role) ||
      role.some((r) => r === req.user.role)
    ) {
      next();
    } else {
      res.status(403).send({msg: 'Access to the resource is forbidden.'});
    }
  };
};
