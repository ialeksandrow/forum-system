/**
 * Checks for request body fields validity.
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} object with validating functions
 * @returns {function} express middleware function
 */

export default (schema) => async (req, res, next) => {
  let ERROR = false;

  Object.keys(schema)
      .filter((key) => Object.keys(req.body).includes(key))
      .forEach((key) => {
        if (!(schema[key](req.body[key]))) {
          ERROR = true;

          return res.status(400).json({msg: `Server rejects '${req.body[key]}' to be a valid value for '${key}' field.`});
        }
      });

  if (!ERROR) {
    await next();
  }
};
