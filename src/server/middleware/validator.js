/**
 * Checks for request body fields validity.
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} object with validating functions
 * @returns {function} express middleware function
 */

export default (schema) => async (req, res, next) => {
  if (Object.keys(schema).length !== Object.keys(req.body).length) {
    return res.status(400).json({msg: `Post request body requires ${Object.keys(schema).length} fields. Expected field order: { ${Object.keys(schema).map((key) => `${key}: value`).join(', ')} }`});
  }

  let ERROR = false;

  Object.keys(schema)
      .forEach((key) => {
        if (!(schema[key](req.body[key]))) {
          ERROR = true;

          return res.status(400).json({msg: `Server rejects '${req.body[key]}' to be a valid value for '${key}' field.`});
        }
      });

  if (!ERROR) {
    await next();
  }
};
