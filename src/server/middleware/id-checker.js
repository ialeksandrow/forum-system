/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {object} req
 * @param {object} res
 * @param {function} next
 * @param {number} id
 * @param {string} name
 */

const idChecker = async (req, res, next, id, name) => {
  if (!Number.isInteger(+id)) {
    return res.status(400).json({
      status: '400 - Bad Request',
      errorMessage: 'ids in URL must be integer',
    });
  }

  next();
};

export default idChecker;
