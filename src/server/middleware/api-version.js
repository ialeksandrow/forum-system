export default (req, res, next) => {
  res.set('X-API-Version', '1.0.0');
  next();
};
