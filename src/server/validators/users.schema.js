/**
 * Validation schema for users endpoint with request type 'post'.
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @type {object}
 */

export default {
  username: (value) =>
    typeof value === 'string' && value.length >= 5 && value.length <= 15,
  password: (value) =>
    typeof value === 'string' &&
    value.length >= 5 &&
    value.length <= 255 &&
    value.match(/[A-Z]/) &&
    value.match(/[0-9]/),
  email: (value) =>
    typeof value === 'string' &&
    value.length >= 5 &&
    value.length <= 255 &&
    /([\w\d._]+)@([\w.]+)/.test(value),
  birthdate: (value) =>
    !isNaN(Date.parse(value)) && Date.parse(value) < Date.now(),
};
