/* eslint-disable max-len */

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 */
export const createPostSchema = {
  post_title: (value) =>
    !!value && typeof value === 'string' && value.length <= 255,

  post_content: (value) =>
    !!value && typeof value === 'string' && value.length >= 10,
};

export const updatePostSchema = {
  post_content: (value) =>
    !!value && typeof value === 'string' && value.length >= 10,
};

export const updateUser = {
  username: (value) => !value || value.length<15,
};
