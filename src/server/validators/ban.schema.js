import banDurations from '../common/ban-durations.enum.js';

/**
 * Validation schema for users endpoint with request type 'put', associated with banning functionality.
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @type {object}
 */

export default {
  duration: (value) => Object.keys(banDurations).some((key) => key.toLowerCase() === value.toLowerCase()),
};
