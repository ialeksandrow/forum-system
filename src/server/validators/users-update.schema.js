/**
 * Validation schema for users endpoint with request type 'put'.
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @type {object}
 */

export default {
  password: (value) =>
    typeof value === 'string' &&
    value.length >= 5 &&
    value.length <= 255 &&
    value.match(/[A-Z]/) &&
    value.match(/[0-9]/),
  email: (value) =>
    typeof value === 'string' &&
    value.length >= 5 &&
    value.length <= 255 &&
    /([\w\d._]+)@([\w.]+)/.test(value),
  birthdate: (value) =>
    !isNaN(Date.parse(value)) && Date.parse(value) < Date.now(),
  first_name: (value) =>
    typeof value === 'string' && value.length > 0 && value.length <= 30,
  last_name: (value) =>
    typeof value === 'string' && value.length > 0 && value.length <= 30,
};
