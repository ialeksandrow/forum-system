export const createCommentSchema = {
  comment_content:
    (value) => !!value && typeof value === 'string' && value.length >= 10,
};
