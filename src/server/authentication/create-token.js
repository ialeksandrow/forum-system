import jwt from 'jsonwebtoken';

export default (payload) => {
  const options = {
    expiresIn: 86400,
  };

  return jwt.sign(payload, process.env.PRIVATE_KEY, options);
};
