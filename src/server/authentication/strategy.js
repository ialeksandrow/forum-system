import passportJwt from 'passport-jwt';

const options = {
  secretOrKey: process.env.PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    user_id: payload.user_id,
    username: payload.username,
    first_name: payload.first_name,
    last_name: payload.last_name,
    email: payload.email,
    birthdate: payload.birthdate,
    registration_date: payload.registration_date,
    role: payload.role,
  };

  // userData will be set as 'req.user' in the 'next' middleware
  done(null, userData);
});

export default jwtStrategy;
