import jwtDecode from 'jwt-decode';

/**
 * Decodes JWT token from server
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} token to decode
 * @returns {object} jwt decoded token
 */

const decodeToken = token => {
  try {
    return jwtDecode(token);
  } catch {
    localStorage.removeItem('token');
    return null;
  }
};

export default decodeToken;
