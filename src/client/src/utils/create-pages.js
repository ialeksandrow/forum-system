const createPages = dataLength => {
  const pagesArr = [];

  for (let i = 1; i <= dataLength; i++) {
    pagesArr.push(i);
  }

  return pagesArr;
};

export default createPages;
