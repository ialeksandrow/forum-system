/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {string} username 
 * @param {object} accountState 
 * @returns {boolean}
 */
const isAuthorized = (username, accountState) =>
  accountState.username === username || accountState.role === 'admin';

export default isAuthorized;
