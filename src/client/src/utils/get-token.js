/**
 * Gets token either from user's local or session storage
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {string}
 */

const getToken = () => {
  const token =
    localStorage.getItem('token') || sessionStorage.getItem('token');

  return token;
};

export default getToken;
