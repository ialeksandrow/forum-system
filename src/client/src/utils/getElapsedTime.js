/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {string} date
 * @returns {string}
 */
const getElapsedTime = (date) => {
  const commentDate = new Date(date);
  const now = new Date();
  const interval = (now.getTime() - commentDate.getTime()) / 1000 / 60;

  const result = [];

  if (interval < 60) {
    result.push(Math.floor(interval), ' minute');
  } else if (interval < 24 * 60) {
    result.push(Math.floor(interval / 60), ' hour');
  } else {
    result.push(Math.floor(interval / 60 / 24), ' day');
  }

  result.push(result[0] > 1 ? 's': '');
  
  return result.join('');
};

export default getElapsedTime;
