/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {string} input 
 * @param {object} validations 
 * @returns {boolean}
 */
const isInputValid = (input, validations) => {
  let isValid = true;

  if (validations.isRequired) {
    isValid = isValid && input.length !== 0;
  }

  if (validations.minLength) {
    isValid = isValid && input.length >= validations.minLength;
  }

  if (validations.maxLength) {
    isValid = isValid && input.length <= validations.maxLength;
  }

  return isValid;
};

export default isInputValid;
