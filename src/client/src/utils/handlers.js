/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 *
 * @param {number} user_id 
 * @param {number} user_id_compare 
 * @param {string} modalId 
 */
export const handleMouseEnterBtn = (user_id, user_id_compare, modalId) => {
  if (user_id === user_id_compare) {
    document
    .getElementById('edit-button-' + modalId)
    .classList.remove('button-hidden');
  }
};

export const handleMouseLeaveBtn = (user_id, user_id_compare, modalId) => {
  if (user_id === user_id_compare) {
    document
    .getElementById('edit-button-' + modalId)
    ?.classList.add('button-hidden');
  }
};
