import './ErrorView.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @returns {Component}
 */
const ErrorView = () => {
  return (
    <div className='d-flex justify-content-center mt-5'>
      <div className='card mx-5 mx-md-5 shadow'>
        <div className='card-body text-center'>
          <p className='lead'>Oops, something went wrong :/</p>
          <p>
            We apologise for the inconvenience. <br /> Our team of passionate
            developers is working hard to restore the Forum.
          </p>

          <i className='bi bi-bug-fill fs-1'></i>
        </div>
      </div>
    </div>
  );
};

export default ErrorView;
