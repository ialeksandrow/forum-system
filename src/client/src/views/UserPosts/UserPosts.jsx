import './UserPosts.css';
import htmlReactParser from 'html-react-parser';
import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from './../../Context/AuthContext';
import getToken from './../../utils/get-token';
import { BASE_URL } from './../../common/constants';
import UserTopBar from './../../components/UserTopBar/UserTopBar';
import Pagination from '../../components/Pagination/Pagination';

/**
 * Posts history page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const UserPosts = () => {
  const { accountState } = useContext(AuthContext);
  const [posts, setPosts] = useState(null);
  const [postsCount, setPostsCount] = useState(0);
  const [postsPerPage, setPostsPerPage] = useState(6);
  const [currentPage, setCurrentPage] = useState(1);

  const lastPostIndex = currentPage * postsPerPage;
  const firstPostIndex = lastPostIndex - postsPerPage;

  useEffect(() => {
    fetch(`${BASE_URL}/users/${accountState.user_id}/posts`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          console.log(resData.msg);
        }

        setPostsCount(resData.data.results.length);

        return setPosts(
          resData.data.results.slice(firstPostIndex, lastPostIndex)
        );
      })
      .catch(_ => console.log('Something went wrong!'));
  }, [postsPerPage, currentPage]);

  if (posts && posts.length === 0) {
    return (
      <div className="container mt-5 mb-5">
        <UserTopBar />
        <div className="row mt-5">
          <div className="col">
            <div className="text-secondary">
              It's lonely here. Post something on the forum to see results.
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="container mt-5 mb-5">
      <UserTopBar />
      <div className="row mt-5">
        <div className="col-md-8">
          <p className="fs-3 mb-3">Posts Activity</p>
        </div>
        <div className="col-md-4">
          <label htmlFor="range" className="form-label">
            Posts Per Page
          </label>
          <input
            type="range"
            min="6"
            max="12"
            step="3"
            className="form-range"
            id="range"
            value={postsPerPage}
            onChange={e => {
              setPostsPerPage(parseInt(e.target.value));
            }}
          />
        </div>
      </div>
      {posts ? (
        <div className="row g-5 mt-1">
          {posts.map(post => {
            return (
              <div className="post-box col-md-6" key={post.post_id}>
                <div className="post-box__top">
                  <div className="post-box__date">
                    {new Date(post.post_date).toLocaleString('en-GB')}
                  </div>
                  <Link
                    to={`/posts/${post.post_id}`}
                    className="post-box__title"
                  >
                    {post.post_title}
                  </Link>
                  <div className="post-box__views">
                    <i className="bi bi-eye"></i>
                    {post.post_views}
                  </div>
                </div>
                <div className="post-box__body">
                  {htmlReactParser(post.post_content.substring(0, 300))}...
                </div>
              </div>
            );
          })}
          <Pagination
            dataLength={postsCount}
            dataPerPage={postsPerPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
      ) : null}
    </div>
  );
};

export default UserPosts;
