import { useState, useEffect } from 'react';
import { BASE_URL, BAN_DURATIONS } from '../../common/constants';
import getToken from './../../utils/get-token';
import UserTopBar from '../../components/UserTopBar/UserTopBar';
import ProfileStats from './../../components/ProfileStats/ProfileStats';
import UserProfiles from './../../components/UserProfiles/UserProfiles';

/**
 * UserPenalties page component (admin)
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const UserPenalties = () => {
  const [username, setUsername] = useState('');
  const [user, setUser] = useState(null);
  const [error, setError] = useState('');
  const [banDuration, setBanDuration] = useState(null);
  const [banExpiration, setBanExpiration] = useState('');

  useEffect(() => {
    fetch(`${BASE_URL}/users/?q=${username}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(res => res.json())
      .then(resData => {
        setError('');

        if (resData.msg) {
          return setError(resData.msg);
        }

        return setUser(resData.data.results[0]);
      })
      .catch(err => console.log(err));
  }, [username]);

  const showAlert = () => {
    return (
      <div className="alert alert-success" role="alert">
        Account successfully banned. User will be able to log in after{' '}
        <strong>{banExpiration}</strong>.
      </div>
    );
  };

  if (banExpiration) {
    setTimeout(() => setBanExpiration(''), 5000);
  }

  const userBan = e => {
    e.preventDefault();

    if (!banDuration) {
      return setError('Ban duration is required.');
    }

    if (user) {
      fetch(`${BASE_URL}/users/${user.user_id}/ban`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${getToken()}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          duration: banDuration
        })
      })
        .then(res => res.json())
        .then(resData => {
          if (resData.msg) {
            return setError(resData.msg);
          }

          return setBanExpiration(
            new Date(resData.data.results[0].ban_expiration).toLocaleString(
              'en-GB'
            )
          );
        })
        .catch(err => console.log(err));
    }
  };

  return (
    <div className="container mt-5 mb-5">
      <UserTopBar />
      <div className="row mt-5">
        <div className="col-md-4 d-flex flex-column justify-content-center align-items-center mb-5">
          {user && username && !error ? (
            <ProfileStats id={user.user_id} />
          ) : (
            <>
              <div className="spinner-grow text-primary mb-3" role="status">
                <span className="visually-hidden">Searching for a user...</span>
              </div>
              <div className="text-secondary">Searching for a user...</div>
            </>
          )}
        </div>
        <div className="col-md-8 d-flex flex-column justify-content-center ps-md-5">
          {banExpiration && showAlert()}
          <p className="fs-3">Ban User Account</p>
          <form onSubmit={userBan}>
            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                type="text"
                name="username"
                id="username"
                className="form-control"
                aria-describedby="usernameHelp"
                placeholder="Enter username"
                onChange={e => setUsername(e.target.value)}
              />
              <div id="usernameHelp" className="form-text">
                Given username will be banned.
              </div>
            </div>
            {error && <p className="text-danger mb-3">{error}</p>}
            {BAN_DURATIONS.map(duration => {
              return (
                <div key={duration} className="form-check mb-3">
                  <label
                    htmlFor={duration}
                    className="form-check-label"
                  >{`${duration[0].toLocaleUpperCase()}${duration.slice(
                    1
                  )}`}</label>
                  <input
                    type="radio"
                    name="banDuration"
                    id={duration}
                    className="form-check-input"
                    value={duration}
                    onChange={e => setBanDuration(e.target.value)}
                  />
                </div>
              );
            })}
            <button type="submit" className="btn btn-danger">
              Ban User
            </button>
          </form>
        </div>
      </div>
      <UserProfiles
        title="Banned Users"
        endpoint="banned"
        newBan={banExpiration}
      />
    </div>
  );
};

export default UserPenalties;
