import './Home.css';
import { useContext } from 'react';
import AuthContext from '../../Context/AuthContext';
import { HOME_ROUTES } from '../../common/constants';
import Latest from '../Posts/Latest';
import Login from './../../components/Login/Login';
import Register from './../../components/Register/Register';

/**
 * Application home page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const Home = ({ history, match, location }) => {
  const { accountState } = useContext(AuthContext);

  return (
    <>
      {accountState ? (
        <Latest location={location} />
      ) : (
        <div className="container vh-100">
          <div className="row h-100 d-flex align-items-center">
            <div className="col-lg-8 col-md-6">
              <div className="home-brand">
                <div className="home-brand__logo">
                  F
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                  >
                    <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z" />
                    <path d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z" />
                  </svg>
                  RUM
                </div>
                <div className="home-brand__subtitle">
                  A place to share experience and better understand our world.
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6">
              {match.path === HOME_ROUTES.login && <Login history={history} />}
              {match.path === HOME_ROUTES.register && (
                <Register history={history} />
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Home;
