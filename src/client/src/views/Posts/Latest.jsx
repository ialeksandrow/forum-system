import React, { useState, useEffect, useContext } from 'react';
import { BASE_URL } from '../../common/constants';
import PostTitleCard from '../../components/Posts/PostTitleCard';
import { SettingsContext } from '../../Context/SettingsContext';
import Paginator from '../../components/Paginator/Paginator';
import getToken from '../../utils/get-token';
import CreatePostBtn from '../../components/Posts/CreatePostBtn';
import { Redirect } from 'react-router-dom';
import CreatePost from '../../components/TextFormMarkdown/CreatePost';
import AuthContext from '../../Context/AuthContext';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const Latest = ({ location }) => {
  const [posts, setPosts] = useState([]);
  const [pagesCount, setPagesCount] = useState(null);
  const [isLoaded, setLoaded] = useState(false);
  const [isError, setError] = useState(false);
  const settings = useContext(SettingsContext);
  const currentPage = +new URLSearchParams(location.search).get('page') || 1;
  const postsPerPage =
    +new URLSearchParams(location.search).get('count') || settings.countPosts;
  const { accountState } = useContext(AuthContext);

  useEffect(() => {
    fetch(`${BASE_URL}/posts?page=${currentPage}&count=${postsPerPage}`, {
      headers: {
        Authorization: `bearer ${getToken()}`,
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        setError(true);
        throw new Error(res.statusText);
      })
      .then((res) => {
          document.title = currentPage > 1 ? `Latest posts | Page ${currentPage} | Forum` : 'Latest posts | Forum'
          setPosts(res.data.result);
          setPagesCount(res.data.pagesCount);
          setLoaded(true);
      })
      .catch((err) => {
        setError(true);
      });
  }, [currentPage, location, postsPerPage]);

  if (isError) {
    return <Redirect to='/error' />
  }

  return (
    isLoaded && (
      <>
        <div className='container mb-5'>
          <div className='d-flex mb-2 flex-row-reverse'>
            <CreatePostBtn />
            <CreatePost username={accountState.username} />
          </div>
          {posts.map((post) => (
            <PostTitleCard key={post.post_id} {...post} />
          ))}
          <Paginator
          param='latest'
          pagesCount={pagesCount}
          page={currentPage}
          itemsPerPage={postsPerPage || settings.countPosts}
        />
        </div>
      </>
    )
  );
};

export default Latest;
