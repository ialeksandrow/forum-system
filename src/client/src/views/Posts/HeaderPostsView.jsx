import Navigation from '../../components/Navigation/Navigation';
import NavigationLink from '../../components/Navigation/NavigationLink';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @returns {Component}
 */
const HeaderPostsView = () => {
  return (
    <div className='container d-flex justify-content-between align-items-center'>
      <Navigation>
        <NavigationLink name='Latest' route='/latest' />
        <NavigationLink name='Trending' route='/trending' />
        <NavigationLink name='Other' route='/other' />
      </Navigation>
    </div>
  );
};

export default HeaderPostsView;
