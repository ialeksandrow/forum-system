import React, { useState, useEffect, useContext } from 'react';
import getToken from '../../utils/get-token';
import { Redirect, useParams } from 'react-router-dom';
import { BASE_URL } from '../../common/constants';
import CommentCard from '../../components/Comments/CommentCard';
import PostFullCard from '../../components/Posts/PostFullCard';
import PostThreadHeader from '../../components/Posts/PostThreadHeader';
import AuthContext from '../../Context/AuthContext';
import EditPost from '../../components/ActionsPostComment/EditPost';
import ReplyPost from '../../components/TextFormMarkdown/ReplyPost';
import EditComment from '../../components/ActionsPostComment/EditComment';
import isAuthorizedFunc from '../../utils/isAuthorized';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @returns {Component}
 */
const Thread = ({ location }) => {
  const { id } = useParams();
  const [post, setPost] = useState([]);
  const [comments, setComments] = useState([]);
  const [isLoaded, setLoaded] = useState(false);
  const [isError, setError] = useState(false);
  const { accountState } = useContext(AuthContext);
  const isAuthorized = isAuthorizedFunc(post.username, accountState);

  const PostEditComponent =
    isLoaded && isAuthorized ? (
      <EditPost
        post={post}
        setPost={setPost}
        isLiked={post.isLiked}
      />
    ) : (
      <div key='empty'></div>
    );

  const CommentEditComponent = (comment) =>
    isLoaded && isAuthorized ? (
      <EditComment
        username={comment.username}
        post_id={id}
        comment={comment}
        comments={comments}
        setComments={setComments}
        isLiked={comment.isLiked}
      />
    ) : (
      <div key='empty'></div>
    );

  useEffect(() => {
    fetch(`${BASE_URL}${location.pathname}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        }

        setError(true);
        throw new Error(res.statusText);
      })
      .then((res) => {
          document.title = res.post.data.post_title
          setPost(res.post.data);
          setComments(res.comments.data);
          setLoaded(true);
      })
      .catch((err) => setError(true));
  }, [id, location.pathname]);

  if (isError) {
    return <Redirect to='/error' />
  }

  if (!isLoaded) {
    return (
      <div className='container'>
        <div className='spinner-border' role='status'>
          <span className='visually-hidden'>Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <>
      <div className='container mb-5'>
        <PostThreadHeader {...post} />

        <PostFullCard {...post} EditComponent={PostEditComponent} />

        <div className='container p-0'>
          {comments.map((comment) => {
            return <CommentCard
              key={comment.comment_id}
              EditComponent={CommentEditComponent(comment)}
              setComments={setComments}
              {...comment}
            />}
          )}
        </div>

        <ReplyPost
          post_id={id}
          username={post.username}
          location={location}
          setComments={setComments}
          comments={comments}
        />
      </div>
    </>
  );
};

export default Thread;
