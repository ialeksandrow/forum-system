import { useContext } from 'react';
import AuthContext from '../../Context/AuthContext';
import { BASE_URL } from '../../common/constants';
import getToken from '../../utils/get-token';
import { Redirect } from 'react-router';
import UserTopBar from '../../components/UserTopBar/UserTopBar';
import ProfileStats from '../../components/ProfileStats/ProfileStats';

/**
 * Personal user's account deletion page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const AccountDeletion = () => {
  const { accountState, setAccountState } = useContext(AuthContext);

  const deleteUserAccount = () => {
    fetch(`${BASE_URL}/users/${accountState.user_id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(res => res.json())
      .then(_ => {
        localStorage.removeItem('token');
        sessionStorage.removeItem('token');

        setAccountState(null);

        return <Redirect to="/" />;
      })
      .catch(_ => console.log(_));
  };

  return (
    <div className="container mt-5 mb-5">
      <UserTopBar />
      <div className="row mt-5">
        <div className="col-md-4 mb-5">
          <ProfileStats id={accountState.user_id} />
        </div>
        <div className="col-md-8 d-flex flex-column justify-content-center align-items-center">
          <p className="fs-3">Account Deletion</p>
          <p className="text-center">
            Deleting your account is an irreversible process, which we can't
            revert even if you perform it by accident.
          </p>
          <button
            className="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#deleteAccountConfirmation"
          >
            Delete My Account
          </button>
          <div
            className="modal fade"
            id="deleteAccountConfirmation"
            aria-labelledby="deleteAccountConfirmationLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered">
              <div className="modal-content">
                <div className="modal-header">
                  <p
                    className="modal-title fs-3"
                    id="deleteAccountConfirmationLabel"
                  >
                    Are you sure?
                  </p>
                </div>
                <div className="modal-body">
                  If you confirm you will be automatically logged out and not
                  able to access your account anymore.
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-bs-dismiss="modal"
                    onClick={deleteUserAccount}
                  >
                    Delete Account
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AccountDeletion;
