import './UserComments.css';
import htmlReactParser from 'html-react-parser';
import { useContext, useEffect, useState } from 'react';
import AuthContext from './../../Context/AuthContext';
import getToken from './../../utils/get-token';
import { BASE_URL } from './../../common/constants';
import UserTopBar from './../../components/UserTopBar/UserTopBar';
import Pagination from '../../components/Pagination/Pagination';

/**
 * Comments history page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const UserComments = () => {
  const { accountState } = useContext(AuthContext);
  const [comments, setComments] = useState(null);
  const [commentsCount, setCommentsCount] = useState(0);
  const [commentsPerPage, setCommentsPerPage] = useState(6);
  const [currentPage, setCurrentPage] = useState(1);

  const lastCommentIndex = currentPage * commentsPerPage;
  const firstCommentIndex = lastCommentIndex - commentsPerPage;

  useEffect(() => {
    fetch(`${BASE_URL}/users/${accountState.user_id}/comments`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          console.log(resData.msg);
        }

        setCommentsCount(resData.data.results.length);

        return setComments(
          resData.data.results.slice(firstCommentIndex, lastCommentIndex)
        );
      })
      .catch(_ => console.log('Something went wrong!'));
  }, [commentsPerPage, currentPage]);

  if (comments && comments.length === 0) {
    return (
      <div className="container mt-5 mb-5">
        <UserTopBar />
        <div className="row mt-5">
          <div className="col">
            <div className="text-secondary">
              It's lonely here. Comment something on the forum to see results.
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="container mt-5 mb-5">
      <UserTopBar />
      <div className="row mt-5">
        <div className="col-md-8">
          <p className="fs-3 mb-3">Comments Activity</p>
        </div>
        <div className="col-md-4">
          <label htmlFor="range" className="form-label">
            Comments Per Page
          </label>
          <input
            type="range"
            min="6"
            max="12"
            step="3"
            className="form-range"
            id="range"
            value={commentsPerPage}
            onChange={e => {
              setCommentsPerPage(parseInt(e.target.value));
            }}
          />
        </div>
      </div>
      {comments ? (
        <div className="row g-5 mt-1">
          {comments.map(comment => {
            return (
              <div
                className="comment-box col-lg-4 col-md-6"
                key={comment.comment_id}
              >
                <div className="comment_box__post-title">
                  <span>Topic:</span>
                  {comment.post_title}
                </div>
                <div className="comment-box__body">
                  <i className="bi bi-arrow-return-right"></i>
                  <div className="comment-box__user-avatar">
                    {accountState.username[0].toUpperCase()}
                  </div>
                  <div className="comment-box__info">
                    <div className="comment-box__comment-content">
                      {htmlReactParser(
                        comment.comment_content.substring(0, 300)
                      )}
                      ...
                    </div>
                    <div className="comment-box__comment-date">
                      {new Date(comment.comment_date).toLocaleDateString(
                        'en-GB'
                      )}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
          <Pagination
            dataLength={commentsCount}
            dataPerPage={commentsPerPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
      ) : null}
    </div>
  );
};

export default UserComments;
