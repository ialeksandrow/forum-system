import UserTopBar from '../../components/UserTopBar/UserTopBar';
import EditProfile from '../../components/EditProfile/EditProfile';

/**
 * Personal user's settings page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const Settings = () => {
  return (
    <div className="container mt-5 mb-5">
      <UserTopBar />
      <EditProfile />
    </div>
  );
};

export default Settings;
