import UserTopBar from './../../components/UserTopBar/UserTopBar';
import UserProfiles from './../../components/UserProfiles/UserProfiles';

/**
 * User friendlist page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const UserFriends = () => {
  return (
    <div className="container mt-5 mb-5">
      <UserTopBar />
      <UserProfiles title="All Friends" endpoint="friends" />
    </div>
  );
};

export default UserFriends;
