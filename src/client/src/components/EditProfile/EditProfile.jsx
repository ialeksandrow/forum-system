import { useState, useEffect, useContext } from 'react';
import AuthContext from './../../Context/AuthContext';
import { BASE_URL } from './../../common/constants';
import getToken from './../../utils/get-token';
import ProfileStats from '../ProfileStats/ProfileStats';

/**
 * Personal edit user's credential page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const EditProfile = ({ id }) => {
  const { accountState, setAccountState } = useContext(AuthContext);
  const [user, setUser] = useState(null);
  const [formCredentials, setFormCredentials] = useState({});
  const [formValidity, setFormValidity] = useState(true);

  useEffect(() => {
    fetch(`${BASE_URL}/users/${id || accountState.user_id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(res => res.json())
      .then(resData => resData.data.results)
      .then(userData => {
        if (userData.msg) {
          return console.log(userData.msg);
        }

        setUser(userData);

        return setFormCredentials({
          password: {
            type: 'password',
            placeholder: 'Enter new password',
            value: '',
            valid: true,
            message:
              'Password must be more than 5 characters long and must contain at least one uppercase letter and non-alphabetic 0 - 9 character.'
          },
          first_name: {
            type: 'text',
            placeholder: userData.first_name || 'Enter your first name',
            value: '',
            valid: true,
            message: 'First name must be between 1 - 30 characters long.'
          },
          last_name: {
            type: 'text',
            placeholder: userData.last_name || 'Enter your last name',
            value: '',
            valid: true,
            message: 'Last name must be between 1 - 30 characters long.'
          },
          email: {
            type: 'email',
            placeholder: userData.email,
            value: '',
            valid: true,
            message: 'Invalid email address.'
          },
          birthdate: {
            type: 'date',
            placeholder: new Date(userData.birthdate).toLocaleDateString(
              'en-GB'
            ),
            value: '',
            valid: true,
            message: 'Invalid date of birth.'
          }
        });
      })
      .catch(_ => console.log('Something went wrong.'));
  }, [id, accountState.user_id]);

  const formInputsValidator = {
    password: value =>
      value.length >= 5 &&
      value.length <= 255 &&
      value.match(/[A-Z]/) &&
      value.match(/[0-9]/),
    first_name: value => value.trim().length > 0 && value.trim().length <= 30,
    last_name: value => value.trim().length > 0 && value.trim().length <= 30,
    email: value =>
      value.length >= 5 &&
      value.length <= 255 &&
      /([\w\d._]+)@([\w.]+)/.test(value),
    birthdate: value =>
      !isNaN(Date.parse(value)) && Date.parse(value) < Date.now()
  };

  const handleInput = e => {
    const { name, value } = e.target;

    const updatedField = formCredentials[name];
    updatedField.value = value;
    updatedField.valid = formInputsValidator[name](value);

    setFormCredentials({ ...formCredentials, [name]: updatedField });

    return setFormValidity(
      Object.values(formCredentials).every(field => field.valid)
    );
  };

  const updateProfile = e => {
    e.preventDefault();

    if (formValidity) {
      const data = Object.entries(formCredentials).reduce(
        (acc, [key, element]) => {
          if (element.value) {
            acc[key] = element.value;
          }

          return acc;
        },
        {}
      );

      fetch(`${BASE_URL}/users/${id || accountState.user_id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${getToken()}`
        },
        body: JSON.stringify(data)
      })
        .then(res => res.json())
        .then(resData => {
          if (resData.msg) {
            return console.log(resData.msg);
          }

          setUser(resData.data);
          setAccountState({
            ...accountState,
            ...resData.data
          });

          const clearInput = Object.keys(formCredentials)
            .filter(key => formCredentials[key].value)
            .reduce((acc, key) => {
              acc[key] = {
                value: '',
                valid: true,
                placeholder: resData.data[key]
              };
              return acc;
            }, {});

          return setFormCredentials({ ...formCredentials, ...clearInput });
        })
        .catch(_ => console.log('Something went wrong.'));

      return setFormValidity(false);
    }
  };

  return (
    <>
      {user ? (
        <div className="row mt-5">
          <div className="col-md-4 mb-5">
            <ProfileStats id={accountState.user_id} />
          </div>
          <div className="col-md-8 d-flex flex-column justify-content-center align-items-center ps-md-5">
            <div className="w-100 d-flex justify-content-between align-items-center">
              <p className="fs-3">My Account</p>
              <button
                type="submit"
                form="edit-profile"
                className={`btn ${
                  formValidity ? 'btn-success' : 'btn-primary disabled'
                }`}
              >
                <i
                  className={`bi ${
                    formValidity ? 'bi-save' : 'bi-pencil-square'
                  } me-1`}
                ></i>
                {formValidity ? 'Save Changes' : 'Make Changes'}
              </button>
            </div>
            <form id="edit-profile" onSubmit={updateProfile}>
              <div className="row">
                <div className="col-md-6 mb-3">
                  <label htmlFor="username" className="form-label">
                    Username
                  </label>
                  <input
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    aria-describedby="usernameHelp"
                    placeholder={user.username}
                    disabled
                  />
                  <div id="usernameHelp" className="form-text">
                    You can't change your username.
                  </div>
                </div>
                {Object.entries(formCredentials).map(([key, element]) => {
                  return (
                    <div key={key} className="col-md-6 mb-3">
                      <label htmlFor={key} className="form-label">
                        {`${key[0].toUpperCase()}${key
                          .slice(1)
                          .replace('_', ' ')}`}
                      </label>
                      <input
                        type={element.type}
                        name={key}
                        id={key}
                        className={`form-control ${
                          !element.valid && 'is-invalid'
                        }`}
                        placeholder={element.placeholder}
                        value={element.value}
                        onChange={handleInput}
                      />
                      {!element.valid && (
                        <div
                          id={`validation${element.name}Feedback`}
                          className="invalid-feedback"
                        >
                          {element.message}
                        </div>
                      )}
                    </div>
                  );
                })}
              </div>
            </form>
          </div>
        </div>
      ) : (
        <div className="row">
          <div className="spinner-border text-primary" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      )}
    </>
  );
};

export default EditProfile;
