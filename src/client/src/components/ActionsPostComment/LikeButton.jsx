import { useLocation } from 'react-router-dom';
import { BASE_URL } from '../../common/constants';
import ErrorToast from '../ErrorToasts/ErrorToast';
import getToken from '../../utils/get-token';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const LikeButton = ({ modalId, type, content_id, isLiked, ...props }) => {
  const location = useLocation();

  const handleLike = (ev) => {
    ev.preventDefault();

    const fetchURL =
      type === 'post'
        ? `${BASE_URL}${location.pathname}/likes`
        : `${BASE_URL}${location.pathname}/comments/${content_id}/likes`;

    const fetchObject = {
      method: isLiked ? 'DELETE' : 'POST',
      headers: {
        Authorization: `bearer ${getToken()}`,
      },
    };

    fetch(fetchURL, fetchObject)
      .then((res) => {
        if (res.ok) {
          return res.json;
        }
        throw new Error(res.statusText);
      })
      .then((res) => {
        if (type === 'post') {
          props.setPost({
            ...props.post,
            post_likes_count: isLiked
              ? props.post.post_likes_count - 1
              : props.post.post_likes_count + 1,
            isLiked: !isLiked,
          });
        }

        if (type === 'comment') {
          props.setComments(
            props.comments.map((item) =>
              +item.comment_id === content_id
                ? {
                    ...props.comment,
                    comment_likes_count: isLiked
                      ? +item.comment_likes_count - 1
                      : +item.comment_likes_count + 1,
                    isLiked: !isLiked,
                  }
                : item
            )
          );
        }
      })
      .catch((err) => {
        document
          .getElementById('like-button-' + modalId)
          .appendChild(
            ErrorToast(modalId, '⚠️ Something went wrong<br />Try again later')
          );
        setTimeout(() => {
          document.getElementById('error-toast-' + modalId).remove();
          document
            .getElementById('confirm-delete-' + modalId)
            .removeAttribute('disabled');
        }, 2500);
      });
  };

  return (
    <div className='round-button no-border liked blue d-flex align-items-center me-1'>
      <button
        className='text-center m-auto p-0 d-flex align-items-center'
        type='button'
        id={'like-button-' + modalId}
        onClick={handleLike}
      >
        {isLiked ? (
          <i className='bi bi-hand-thumbs-up-fill fs-4 d-block' />
        ) : (
          <i className='bi bi-hand-thumbs-up fs-4 d-block' />
        )}
      </button>
    </div>
  );
};

export default LikeButton;
