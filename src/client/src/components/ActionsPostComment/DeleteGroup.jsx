import { useState } from 'react';
import { Redirect, useLocation } from 'react-router-dom';
import { BASE_URL } from '../../common/constants';
import getToken from '../../utils/get-token';
import ErrorToast from '../ErrorToasts/ErrorToast';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const DeleteGroup = ({ modalId, setComments }) => {
  const [isDeleted, setDeleted] = useState(false);

  let isDeleting = false;

  const location = useLocation();
  const [type, id] =
    modalId[0] === 'p'
      ? ['post', modalId.substring(4)]
      : ['comment', modalId.substring(7)];

  const requestConfirmation = () => {
    if (!isDeleting) {
      document
        .getElementById('confirm-delete-' + modalId)
        .classList.remove('hidden');
      document
        .getElementById('confirm-delete-' + modalId)
        .parentElement.classList.add('expanded');
    } else {
      document
        .getElementById('confirm-delete-' + modalId)
        .classList.add('hidden');
      document
        .getElementById('confirm-delete-' + modalId)
        .parentElement.classList.remove('expanded');
    }
  };

  const deleteItem = () => {
    const fetchURL =
      type === 'post'
        ? `${BASE_URL}${location.pathname}`
        : `${BASE_URL}${location.pathname}/comments/${id}`;

    const fetchObject = {
      method: 'DELETE',
      headers: {
        authorization: `bearer ${getToken()}`,
      },
    };

    document
      .getElementById('confirm-delete-' + modalId)
      .setAttribute('disabled', true);

    fetch(fetchURL, fetchObject)
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        throw new Error(res.statusText);
      })
      .then(() => {
        if (type === 'post') {
          document
            .getElementById('edit-button-' + modalId)
            .appendChild(ErrorToast(modalId, 'Post deleted ✅'));
          setTimeout(() => setDeleted(true), 2000);
        }

        if (type === 'comment') {
          document
          .getElementById('edit-button-' + modalId)
          .appendChild(ErrorToast(modalId, 'Comment deleted ✅'));

          setTimeout(() => {
            return setComments((comments) =>
              comments.filter((item) => item.comment_id !== +id)
            );
          }, 2500);
        }
      })
      .catch((err) => {
        document
          .getElementById('edit-button-' + modalId)
          .appendChild(
            ErrorToast(modalId, '⚠️ Something went wrong<br />Try again later')
          );
        setTimeout(() => {
          document.getElementById('error-toast-' + modalId).remove();
          document
            .getElementById('confirm-delete-' + modalId)
            .removeAttribute('disabled');
        }, 2500);
      });
  };

  if (isDeleted) {
    return <Redirect to='/latest' />;
  }

  return (
    <div className='delete-container d-flex align-items-center ms-2'>
      <button
        className='confirm-delete hidden px-2'
        id={'confirm-delete-' + modalId}
        onClick={deleteItem}
      >
        confirm
      </button>

      <div
        className='round-button red d-flex align-items-center'
        id={'delete-button-' + modalId}
      >
        <button
          className='text-center m-auto p-0 d-flex align-items-center'
          type='button'
          id='delete-button'
          onClick={requestConfirmation}
        >
          <i className='bi bi-x fs-1' />
        </button>
      </div>
    </div>
  );
};

export default DeleteGroup;
