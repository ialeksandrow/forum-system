import React from 'react';
import DeleteGroup from './DeleteGroup';
import EditButton from './EditButton';
import ModalEdit from './ModalEdit';
import ActionButtonColumn from './ActionButtonColumn.jsx';
import LikeButton from './LikeButton';
import LikeCounter from './LikeCounter';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const Edit = ({ EditComponent, setComments, modalId }) => {
  const EditComponentWithModalId = { ...EditComponent, modalId };

  const [type, content_id] =
  modalId[0] === 'p'
    ? ['post', +modalId.substring(4)]
    : ['comment', +modalId.substring(7)];

  return (
    <>
      <ActionButtonColumn modalId={modalId}>
        <LikeCounter modalId={modalId} type={type} {...EditComponent.props}   />
        <LikeButton modalId={modalId} type={type} content_id={content_id} {...EditComponent.props} />
        <EditButton modalId={modalId} />
        <DeleteGroup modalId={modalId} setComments={setComments} />
      </ActionButtonColumn>
      <ModalEdit EditComponent={EditComponentWithModalId} modalId={modalId} />
    </>
  );
};

export default Edit;
