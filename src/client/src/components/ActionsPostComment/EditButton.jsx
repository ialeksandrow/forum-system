import React from 'react';
import './EditButton.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const EditButton = ({ modalId }) => {
  return (
    <div
      className='round-button blue d-flex align-items-center'
    >
      <button
        className='text-center m-auto p-0 d-flex align-items-center'
        type='button'
        id='edit-button'
        data-bs-toggle='modal'
        data-bs-target={`#${modalId}`}
      >
        <i className='bi bi-pencil-fill fs-4 d-block' />
      </button>
    </div>
  );
};

export default EditButton;
