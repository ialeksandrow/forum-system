import React from 'react';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const ModalEdit = ({ EditComponent, modalId }) => {
  return (
    <div
      className='modal fade'
      id={modalId}
      tabIndex='-1'
      aria-labelledby='exampleModalLabel'
      aria-hidden='true'
    >
      <div className='modal-dialog modal-fullscreen-md-down'>
        <div className='modal-content rounded border-0'>
          <div className='modal-header'>
            <h5 className='modal-title pt-md-4 ps-md-4' id='exampleModalLabel'>
              Edit
            </h5>
            <button
              type='button'
              className='btn-close'
              id={'btn-close-' + modalId}
              data-bs-dismiss='modal'
              aria-label='Close'
            ></button>
          </div>
          <div className='modal-body'>{EditComponent}</div>
        </div>
      </div>
    </div>
  );
};

export default ModalEdit;
