
/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const LikeCounter = ({modalId, type, content_id, ...props}) => {
  const likes_count = type === 'post' ? props.post.post_likes_count : props.comment.comment_likes_count;

  if (!likes_count) {
    return <></>;
  }

  return(
    <div className='fs-3 ms-3 me-2 p-0'>
      {likes_count}
    </div>
  )
};

export default LikeCounter;
