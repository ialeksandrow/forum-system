/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const ActionButtonColumn = ({ children, modalId }) => {
  return (
    <div
      className='action-buttons-container d-flex align-items-center p-1 button-hidden' 
      id={'edit-button-' + modalId}
    >
      {children}
    </div>
);
};

export default ActionButtonColumn;
