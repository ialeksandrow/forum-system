import React, { useState } from 'react';
import getToken from '../../utils/get-token';
import { BASE_URL } from '../../common/constants';
import MarkdownForm from '../TextFormMarkdown/MarkdownForm';
import './EditItem.css';
import Showdown from 'showdown';
import isInputValid from '../../utils/isInputValid';
import ErrorToast from '../ErrorToasts/ErrorToast';
const converter = new Showdown.Converter();

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const EditComment = ({ post_id, comments, setComments, comment }) => {
  const modalId= 'comment' + comment.comment_id;
  const [isMarkdown, setMarkdown] = useState(true);
  const [parsedMarkdown, setParsedMarkdown] = useState('');
  const [contentTextArea, setContentTextArea] = useState({
    value: converter.makeMarkdown(comment.comment_content),
    placeholder: 'Edit your comment here',
    validation: {
      required: true,
      minLength: 10,
    },
    valid: true,
    touched: false,
  });

  const handleInputChange = (event) => {
    const { value } = event.target;
    const updatedControl = { ...contentTextArea, value, touched: true };

    if (!value.length) {
      updatedControl.touched = !updatedControl.touched && false;
    } else {
      updatedControl.touched = true;
    }

    updatedControl.valid = isInputValid(value, updatedControl.validation);
    setContentTextArea(updatedControl);
  };

  const setValidClass = () => {
    return contentTextArea.valid ? 'valid' : 'invalid';
  };

  const setTouchedClass = () => {
    return !!contentTextArea.touched ? 'touched' : '';
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();

    fetch(`${BASE_URL}/posts/${post_id}/comments/${comment.comment_id}`, {
      body: JSON.stringify({
        comment_content: isMarkdown ? parsedMarkdown : contentTextArea.value,
      }),
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `bearer ${getToken()}`,
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error(res.status);
        }
      })
      .then((res) => {
        const button = document.getElementById('btn-close-comment' + comment.comment_id);
        button.dispatchEvent(new MouseEvent('click'));

        setContentTextArea({
          ...contentTextArea,
          value: converter.makeMarkdown(res.commentContent)
        });

        setComments(
          comments.map((item) =>
            item.comment_id === comment.comment_id
              ? {
                ...comment,
                  comment_content: res.commentContent,
                  comment_date: new Date(),
                  comment_history: res.commentHistory,
                  isNew: true,
                }
              : item
          )
        );
      })
      .catch((err) => {
        document
          .getElementById('submit-button-' + modalId)
          .prepend(
            ErrorToast(modalId, '⚠️ Something went wrong<br />Try again later')
          );
        setTimeout(() => {
          document.getElementById('error-toast-' + modalId).remove();
        }, 4000);
      });
  };

  return (
    <MarkdownForm
      {...{
        handleSubmit,
        setValidClass,
        setTouchedClass,
        contentTextArea,
        handleInputChange,
        isMarkdown,
        setMarkdown,
        setParsedMarkdown,
        username: comment.username,
      }}
    />
  );
};

export default EditComment;
