import './ErrorToast.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {number} modalId
 * @param {string} message
 * @returns {HTML element}
 */
const ErrorToast = (modalId, message) => {
  const toastContainer = document.createElement('div');
  toastContainer.className = 'error-popover p-2';
  toastContainer.id = 'error-toast-' + modalId;

  const toastMessage = document.createElement('p');
  toastMessage.className = 'm-0';
  toastMessage.innerHTML = message;

  toastContainer.appendChild(toastMessage);

  return toastContainer;
};

export default ErrorToast;
