import React from 'react';
import { Link } from 'react-router-dom';
import LeftPaginator from './LeftPaginator';
import './Paginator.css';
import RightPaginator from './RightPaginator';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const Paginator = ({ param, pagesCount, page, itemsPerPage }) => {
  if (pagesCount > 5) {
    const pageNumbers = (() => {
      if (page <= 3) {
        return [1, 2, 3, 4, 5];
      }

      if (page >= pagesCount - 1) {
        const tempArray = Array.from({ length: 5 }).map(
          (_, i) => i + pagesCount - 4
        );
        return tempArray;
      }

      return Array.from({ length: 5 }).map((_, i) => i + page - 2);
    })();

    return (
      <nav aria-label='Page navigation'>
        <ul className='pagination justify-content-center mt-5'>
          <LeftPaginator page={page} itemsPerPage={itemsPerPage} />

          {pageNumbers.map((number) => {
            return (
              <li
                key={number}
                className={`page-item ${number === page && 'disabled'}`}
              >
                <Link
                  className={'page-link'}
                  name={number}
                  to={`/${param}?page=${number}&count=${itemsPerPage}`}
                >
                  {number}
                </Link>
              </li>
            );
          })}

          <RightPaginator
            page={page}
            pagesCount={pagesCount}
            itemsPerPage={itemsPerPage}
          />
        </ul>
      </nav>
    );
  }

  const pageNumbers = Array.from({ length: pagesCount }).map((_, i) => i + 1);

  return (
    <nav aria-label='Page navigation'>
      <ul className='pagination justify-content-center mt-2'>
        <LeftPaginator page={page} itemsPerPage={itemsPerPage} />

        {pageNumbers.map((number) => {
          return (
            <li
              key={number}
              className={`page-item ${number === page && 'disabled'}`}
            >
              <Link
                className={'page-link'}
                name={number}
                to={`/${param}?page=${number}&count=${itemsPerPage}`}
              >
                {number}
              </Link>
            </li>
          );
        })}

        <RightPaginator
          page={page}
          pagesCount={pagesCount}
          itemsPerPage={itemsPerPage}
        />
      </ul>
    </nav>
  );
};

export default Paginator;
