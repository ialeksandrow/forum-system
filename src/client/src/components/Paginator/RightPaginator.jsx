import React from 'react';
import { Link } from 'react-router-dom';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const RightPaginator = ({ page, pagesCount, itemsPerPage }) => {
  return (
    <>
      {page !== pagesCount && (
        <li className={`page-item ${page === pagesCount && 'disabled'}`}>
          <Link
            className='page-link'
            name='next'
            to={`/latest?page=${page + 1}&count=${itemsPerPage}`}
          >
            <span aria-hidden='true'>&raquo;</span>
          </Link>
        </li>
      )}
    </>
  );
};

export default RightPaginator;
