import React from 'react';
import { Link } from 'react-router-dom';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const LeftPaginator = ({ page, itemsPerPage }) => {
  return (
    <>
      {page !== 1 && (
        <li className={`page-item`}>
          <Link
            className='page-link'
            to={`/latest?page=${page - 1}&count=${itemsPerPage}`}
          >
            <span aria-hidden='true'>&laquo;</span>
          </Link>
        </li>
      )}
    </>
  );
};

export default LeftPaginator;
