import React, { useContext, useState } from 'react';
import parse from 'html-react-parser';
import getElapsedTime from '../../utils/getElapsedTime';
import History from '../Posts/History';
import './CommentCard.css';
import AuthContext from '../../Context/AuthContext';
import Edit from '../ActionsPostComment/Index';
import isAuthorizedFunc from '../../utils/isAuthorized';
import ProfileStatsLink from '../ProfileStats/ProfileStatsLink';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const CommentCard = ({
  user_id,
  comment_id,
  comment_content,
  comment_date,
  username,
  comment_history,
  isNew,
  setComments,
  EditComponent = <></>,
}) => {
  const [isOpen, setOpen] = useState(false);
  const { accountState } = useContext(AuthContext);
  const isAuthorized = isAuthorizedFunc(username, accountState);

  const handleMouseEnterBtn = () => {
    if (isAuthorized) {
      document
        .getElementById('edit-button-comment' + comment_id)
        .classList.remove('button-hidden');
    }
  };

  const handleMouseLeaveBtn = () => {
    if (isAuthorized) {
      document
        .getElementById('edit-button-comment' + comment_id)
        .classList.add('button-hidden');
    }
  };

  return (
    <div className='row'>
      <div className='col-lg-1' />
      <div className='col-lg-2 ps-4 ps-lg-0 text-lg-end '>
        <div className='d-none d-lg-block border-top' />
        <div className='py-2'>
          <ProfileStatsLink user_id={user_id} username={username} />
          <br />
          <span>{getElapsedTime(comment_date)} ago</span>
          <br />
          {!!comment_history.length && (
            <>
              <a
                className='fst-italic text-muted'
                data-bs-toggle='collapse'
                role='button'
                href={'#comment-history-' + user_id}
                onClick={() => setOpen(!isOpen)}
              >
                {isOpen ? 'Hide' : 'View edit history'}
              </a>
            </>
          )}
        </div>
      </div>
      <div className='col'>
        {!!comment_history.length && (
          <div className='collapse pb-4' id={'comment-history-' + user_id}>
            <History item_history={comment_history} />
          </div>
        )}

        <div className='col'>
          <div
            className={
              'card shadow-sm mb-2 p-sm-2 rounded' +
              (isNew ? ' new-comment' : '')
            }
            onMouseEnter={handleMouseEnterBtn}
            onMouseLeave={handleMouseLeaveBtn}
          >
            <div className='card-body thread-card d-flex align-items-center'>
              {EditComponent.key !== 'empty' && (
                <Edit
                  EditComponent={EditComponent}
                  setComments={setComments}
                  modalId={'comment' + comment_id}
                />
              )}
              <div className='card-text'>
                <div className='mb-0 comment-text-container'>
                  {parse(comment_content)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CommentCard;
