import React from 'react';
import './MarkdownBtn.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const MarkdownBtn = ({ isMarkdown, setMarkdown }) => {
  const handleClick = () => {
    setMarkdown(!isMarkdown);
  };

  return isMarkdown ? (
    <>
      <button className='text-center m-auto p-0' onClick={handleClick} type='button'>
        <i className='bi bi-markdown-fill fs-2' />
      </button>
    </>
  ) : (
    <button className='text-center m-auto p-0 text-muted' onClick={handleClick} type='button'>
      <i className='bi bi-markdown fs-2' />
    </button>
  );
};

export default MarkdownBtn;
