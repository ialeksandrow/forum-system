import React, { useState } from 'react';
import getToken from '../../utils/get-token';
import { BASE_URL } from '../../common/constants';
import MarkdownForm from './MarkdownForm';
import { Redirect } from 'react-router-dom';
import ErrorToast from '../ErrorToasts/ErrorToast';
import './CreatePost.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const CreatePost = ({ username }) => {
  const modalId= 'newpost';
  const [isMarkdown, setMarkdown] = useState(false);
  const [parsedMarkdown, setParsedMarkdown] = useState('');
  const [idNewPost, setIdNewPost] = useState(null);

  const [titleTextArea, setTitleTextAre] = useState({
    value: '',
    placeholder: 'Enter a descriptive title of at least 10 characters ',
    validation: {
      required: true,
      minLength: 10,
      maxLength: 255,
    },
    valid: false,
    touched: false,
  });
  const [contentTextArea, setContentTextArea] = useState({
    value: '',
    placeholder: 'Blog about whatever you want here :)',
    validation: {
      required: true,
      minLength: 10,
    },
    valid: false,
    touched: false,
  });

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }

    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }

    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };

  const handleChange = (state, setState) => (event) => {
    const value = event.target.value;
    const updatedControl = {
      ...state,
      value,
      touched: value.length ? true : false,
    };

    updatedControl.valid = isInputValid(value, updatedControl.validation);
    setState(updatedControl);
  };

  const setValidClass = (state) => {
    return state.valid ? 'valid' : 'invalid';
  };

  const setTouchedClass = (state) => {
    return !!state.touched ? 'touched' : '';
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();

    fetch(`${BASE_URL}/posts`, {
      body: JSON.stringify({
        post_title: titleTextArea.value,
        post_content: isMarkdown ? parsedMarkdown : contentTextArea.value,
      }),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `bearer ${getToken()}`,
      },
    })
      .then( (res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error(res.statusText);
        }
      })
      .then((res) => {
        setIdNewPost(res.insertId);
      })
      .catch( (err) => {
        document
          .getElementById('submit-button-' + modalId)
          .prepend(
            ErrorToast(modalId, '⚠️ Something went wrong<br />Try again later')
          );
        setTimeout(() => {
          document.getElementById('error-toast-' + modalId).remove();
        }, 4000);      });
  };

  if (idNewPost) {
    return <Redirect to={'/posts/' + idNewPost} />;
  }

  return (
    <>
      <div
        className='modal fade'
        id='create-post-modal'
        tabIndex='-1'
        aria-hidden='true'
      >
        <div className='modal-dialog modal-lg modal-fullscreen-sm-down'>
          <div className='modal-content'>
            <div className='p-4'>
              <textarea
                className={`form-control text-secondary ${setValidClass(
                  titleTextArea
                )} ${setTouchedClass(titleTextArea)}`}
                value={titleTextArea.value}
                placeholder={titleTextArea.placeholder}
                onChange={handleChange(titleTextArea, setTitleTextAre)}
              ></textarea>
            </div>
            <MarkdownForm
              {...{
                modalId,
                handleSubmit,
                setValidClass,
                setTouchedClass,
                contentTextArea,
                handleInputChange: handleChange(
                  contentTextArea,
                  setContentTextArea
                ),
                isMarkdown,
                setMarkdown,
                setParsedMarkdown,
                username,
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default CreatePost;
