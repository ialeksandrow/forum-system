import React from 'react';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const ReplyToHeader = ({ username, ...props }) => {

  return (
    <div className='card shadow-sm mt-5 p-0 rounded'>
      <h3 className='fw-light p-4 pb-2 text-secondary'>
        Reply to {username}
      </h3>
      {props.children}
    </div>
  );
};

export default ReplyToHeader;
