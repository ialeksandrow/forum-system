import React from 'react';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const SubmitCommentBtn = ({ textArea, modalId }) => {
  return textArea.valid ? (
    <button
      className='text-center p-0 active-md-btn'
      type='submit'
      id={'submit-button-' + modalId}
      data-bs-dismiss="modal"
    >
      <span className='align-middle'>Submit</span>
      <i className='bi bi-arrow-up-right fs-4' />
    </button>
  ) : (
    <button className='text-center text-muted p-0' type='submit' disabled>
      <span className='align-middle'>&#8804; 10 chars</span>
    </button>
  );
};

export default SubmitCommentBtn;
