import React from 'react';
import MarkdownBtn from './MarkdownBtn';
import MarkdownPreview from './MarkdownPreview';
import SubmitCommentBtn from './SubmitCommentBtn';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const MarkdownForm = (props) => {
  return (
    <>
      {props.isMarkdown && props.contentTextArea.valid && (
        <MarkdownPreview
          value={props.contentTextArea.value}
          setParsedMarkdown={props.setParsedMarkdown}
        />
      )}
      <form className='d-flex ps-4 pb-4' onSubmit={props.handleSubmit}>
        <textarea
          className={`form-control text-secondary ${props.setValidClass(
            props.contentTextArea
          )} ${props.setTouchedClass(props.contentTextArea)}`}
          value={props.contentTextArea.value}
          placeholder={props.contentTextArea.placeholder}
          onChange={props.handleInputChange}
        ></textarea>
        <div className='col px-4 text-center'>
          <SubmitCommentBtn textArea={props.contentTextArea} modalId={props.modalId} />
          <MarkdownBtn
            isMarkdown={props.isMarkdown}
            isValid={props.contentTextArea.valid}
            setMarkdown={props.setMarkdown}
          />
        </div>
      </form>
    </>
  );
};

export default MarkdownForm;
