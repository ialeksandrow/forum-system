import marked from 'marked';
import parse from 'html-react-parser';
import React from 'react';
import DOMPurify from 'dompurify';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const MarkdownPreview = ({ value, setParsedMarkdown }) => {
  const parsed = marked(value);
  const clean = DOMPurify.sanitize(parsed);

  setTimeout(() => setParsedMarkdown(clean), 100)

  return (
    <div className='card rounded m-4 mt-0 p-2'>{parse(clean)}</div>
  )
};

export default MarkdownPreview;
