import React, { useContext, useState } from 'react';
import getToken from '../../utils/get-token';
import AuthContext from '../../Context/AuthContext';
import { BASE_URL } from '../../common/constants';
import './ReplyPost.css';
import MarkdownForm from './MarkdownForm';
import ReplyToHeader from './ReplyToHeader';
import ErrorToast from '../ErrorToasts/ErrorToast';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const ReplyPost = ({ username, post_id, comments, setComments }) => {
  const modalId= 'post' + post_id;
  const { accountState } = useContext(AuthContext);
  const [isMarkdown, setMarkdown] = useState(false);
  const [parsedMarkdown, setParsedMarkdown] = useState('');
  const [contentTextArea, setContentTextArea] = useState({
    value: '',
    placeholder: 'Type your comment...',
    validation: {
      required: true,
      minLength: 10,
    },
    valid: false,
    touched: false,
  });

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.isRequired) {
      isValid = isValid && input.length !== 0;
    }

    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }

    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };

  const handleInputChange = (event) => {
    const { value } = event.target;
    const updatedControl = { ...contentTextArea, value, touched: true };

    if (!value.length) {
      updatedControl.touched = !updatedControl.touched && false;
    } else {
      updatedControl.touched = true;
    }

    updatedControl.valid = isInputValid(value, updatedControl.validation);
    setContentTextArea(updatedControl);
  };

  const setValidClass = () => {
    return contentTextArea.valid ? 'valid' : 'invalid';
  };

  const setTouchedClass = () => {
    return !!contentTextArea.touched ? 'touched' : '';
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();

    fetch(`${BASE_URL}/posts/${post_id}/`, {
      body: JSON.stringify({
        comment_content: isMarkdown ? parsedMarkdown : contentTextArea.value,
      }),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `bearer ${getToken()}`,
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error(res.status);
        }
      })
      .then((res) => {
        setContentTextArea({ ...contentTextArea, value: '' });

        const scrollTop = document.documentElement.scrollTop;
        const postFullCardBottom = document
          .getElementById('post-full-card')
          .getBoundingClientRect().bottom;

        window.scrollTo(0, scrollTop + postFullCardBottom);

        setComments([
          {
            username: accountState.username,
            comment_id: res.insertId,
            comment_content: res.commentContent,
            comment_date: new Date(),
            comment_history: [],
            isNew: true,
          },
          ...comments,
        ]);
      })
      .catch((err) => {
        document
          .getElementById('submit-button-' + modalId)
          .prepend(
            ErrorToast(modalId, '⚠️ Something went wrong<br />Try again later')
          );
        setTimeout(() => {
          document.getElementById('error-toast-' + modalId).remove();
        }, 4000);
      });
  };

  return (
    <ReplyToHeader username={username}>
      <MarkdownForm
        {...{
          modalId,
          handleSubmit,
          setValidClass,
          setTouchedClass,
          contentTextArea,
          handleInputChange,
          isMarkdown,
          setMarkdown,
          setParsedMarkdown,
          username,
        }}
      />
    </ReplyToHeader>
  );
};

export default ReplyPost;
