import React, { useContext } from 'react';
import parse from 'html-react-parser';
import Edit from '../ActionsPostComment/Index';
import AuthContext from '../../Context/AuthContext';
import './PostFullCard.css';
import isAuthorizedFunc from '../../utils/isAuthorized';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const PostFullCard = ({
  username,
  post_id,
  post_content,
  isNew,
  EditComponent,
}) => {
  const { accountState } = useContext(AuthContext);
  const isAuthorized = isAuthorizedFunc(username, accountState);

  const handleMouseEnterBtn = () => {
    if (isAuthorized) {
      document
        .getElementById('edit-button-post' + post_id)
        .classList.remove('button-hidden');
    }
  };

  const handleMouseLeaveBtn = () => {
    if (isAuthorized) {
      document
        .getElementById('edit-button-post' + post_id)
        .classList.add('button-hidden');
    }
  };

  return (
    <div
      className={
        'card shadow-sm mb-3 p-sm-2 rounded' + (isNew ? ' new-comment' : '')
      }
      id='post-full-card'
      onMouseEnter={handleMouseEnterBtn}
      onMouseLeave={handleMouseLeaveBtn}
    >
      <div className='card-body thread-card d-flex align-items-center'>
        {EditComponent.key !== 'empty' && (
          <Edit EditComponent={EditComponent} modalId={'post' + post_id} />
        )}
        <div className='card-text'>{parse(post_content)}</div>
      </div>
    </div>
  );
};

export default PostFullCard;
