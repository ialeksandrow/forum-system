import React from 'react';
import parse from 'html-react-parser';
import getElapsedTime from '../../utils/getElapsedTime';
import './PostDetailedComponent.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const History = ({ item_history = [] }) => {
  if (!item_history.length) return <></>;

  const millisec = new Date().getTime();
  const isPost = Object.keys(item_history[0]).includes('updated_post');

  const correctOne = () => (isPost ? 'updated_post' : 'updated_comment');

  return (
    <div
      id={'history' + millisec}
      className='carousel carousel-dark p-3 p-md-4 rounded'
      data-bs-ride='carousel'
      data-bs-interval='false'
    >
      <div className='carousel-indicators'>
        {item_history.map((item, index) => (
          <button
            key={item.update_date}
            type='button'
            data-bs-target={'#history' + millisec}
            data-bs-slide-to={index}
            className={+index === 0 ? 'active' : ''}
            aria-current={+index === 0 ? 'true' : ''}
            aria-label={'Slide ' + (index + 1)}
          />
        ))}
      </div>

      <div className='carousel-inner'>
        {item_history.map((item, index) => {
          return (
            <div
              className={'carousel-item' + (+index === 0 ? ' active' : '')}
              key={item.update_date}
            >
              <span className='text-muted'>
                Edit made {getElapsedTime(item.update_date) + ' ago'}
              </span>
              <div className=' w-100 p-2 p-md-4'>
                {parse(item[correctOne()])}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default History;
