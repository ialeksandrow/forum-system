import React from 'react';
import './PostCounterSmall.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const PostCounterSmall = ({ post_comments_count, post_likes_count }) => {
  const renderTextColor = (count) =>
    count ? 'bg-blue-light' : 'bg-muted';

  return (
    <div className='d-flex pt-1 gx-2 d-md-none'>
      <div className='pe-2'>
        <div className={`badge ${renderTextColor(post_comments_count)} rounded-pill`}>
          <i className='bi bi-chat-right-dots pe-2 align-middle'></i>
          <span className='fw-normal align-middle'>{post_comments_count}</span>
        </div>
      </div>

      <div>
        <div className={`badge ${renderTextColor(post_likes_count)} rounded-pill ps-2`}>
          <i className='bi bi-hand-thumbs-up pe-2 align-middle'></i>
          <span className='fw-normal align-middle'>{post_likes_count}</span>
        </div>
      </div>
    </div>
  );
};

export default PostCounterSmall;
