import React, { useState } from 'react';
import getElapsedTime from '../../utils/getElapsedTime';
import ProfileStatsLink from '../ProfileStats/ProfileStatsLink';
import History from './History';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const PostThreadHeader = ({
  username,
  post_title,
  post_date,
  post_history,
  user_id,
}) => {
  const [isHistoryOpen, setHistoryOpen] = useState(false);

  return (
    <div className='m-3'>
      <div>
        <h2>{post_title}<span></span></h2>
      </div>
      <div>
        <ProfileStatsLink user_id={user_id} username={username} />

        <span className='ps-3'>{getElapsedTime(post_date)} ago</span>
        {!!post_history.length && (
          <a
            className='fst-italic text-muted d-block d-sm-inline ps-sm-3'
            data-bs-toggle='collapse'
            role='button'
            href='#history'
            onClick={() => setHistoryOpen(!isHistoryOpen)}
          >
            {isHistoryOpen ? 'Hide' : 'View edit history'}
          </a>
        )}
        
        {!!post_history.length && (
          <div className='collapse pb-4 pt-3' id='history'>
            <History item_history={post_history} />
          </div>
        )}
      </div>
    </div>
  );
};

export default PostThreadHeader;
