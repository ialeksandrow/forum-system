import React from 'react';
import './PostCounterBig.css';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const PostCounterBig = ({ post_comments_count, post_likes_count }) => {
  const renderTextColor = (count) =>
    count ? 'text-blue-light' : 'text-muted';

  return (
    <div className='d-none d-md-block post-counter-big border-end'>
      <div className='d-flex gx-3 me-0' style={{minWidth: '108px'}}>
        <div className='rounded-3 p-2 me-2 '>
          <span className={`pe-2 fs-5 ${renderTextColor(post_comments_count)}`}>
            {post_comments_count}
          </span>
          <i
            className={`bi bi-chat-left-dots-fill fs-6 ${renderTextColor(
              +post_comments_count
            )} align-baseline`}
          />
        </div>

        <div className='rounded-3 p-2'>
          <span className={`pe-2 fs-5 ${renderTextColor(post_likes_count)}`}>
            {post_likes_count}
          </span>
          <i
            className={`bi bi-hand-thumbs-up-fill fs-6 ${renderTextColor(
              post_likes_count
            )} align-baseline`}
          />
        </div>
      </div>
    </div>
  );
};

export default PostCounterBig;
