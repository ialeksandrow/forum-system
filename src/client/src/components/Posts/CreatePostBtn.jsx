/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @returns {Component}
 */
const CreatePostBtn = () => {
  return (
    <div>
      <button
        type='button'
        className='btn btn-success'
        data-bs-toggle='modal'
        data-bs-target='#create-post-modal'
      >
        <i className='bi bi-plus'></i>
        <span>Create a post</span>
      </button>
    </div>
  );
};

export default CreatePostBtn;
