import React from 'react';
import './PostTitleCard.css';
import PostCounterBig from './PostCounterBig';
import PostCounterSmall from './PostCounterSmall';
import { Link } from 'react-router-dom';
import getElapsedTime from '../../utils/getElapsedTime';
import ProfileStatsLink from '../ProfileStats/ProfileStatsLink';

/**
 * @author Louis Kendem <louis.lora-ronco.a29@learn.telerikacademy.com>
 * @param {*} props
 * @returns {Component}
 */
const PostTitleCard = ({
  user_id,
  post_id,
  post_title,
  username,
  post_date,
  post_likes_count,
  post_comments_count,
  is_edited,
}) => {
  return (
    <div className='card shadow-sm mb-2 p-2 rounded'>
      <div className='d-flex'>
        <PostCounterBig
          post_comments_count={+post_comments_count}
          post_likes_count={+post_likes_count}
        />

        <div className='flex-grow-1 d-md-flex ps-sm-3 flex-fill align-items-center'>
          <div className='col flex-grow-1'>
            <h5 className='m-0 mt-sm-1'>
              <Link className='text-reset' to={`/posts/${post_id}`}>
                {post_title}
              </Link>
            </h5>

            <div className='ms-3'>
              <PostCounterSmall
                post_comments_count={+post_comments_count}
                post_likes_count={+post_likes_count}
              />
            </div>
          </div>

          <div className='col'>
            <div className='mb-0 pe-2 text-md-end'>
              <span className='post-date-info fw-light text-secondary mx-1'>
                {is_edited
                  ? `Modified ${getElapsedTime(post_date)} ago by`
                  : `Created ${getElapsedTime(post_date)} ago by`}
              </span>
              <ProfileStatsLink user_id={user_id} username={username} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PostTitleCard;
