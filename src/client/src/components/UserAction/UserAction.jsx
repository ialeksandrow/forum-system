import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { BASE_URL } from './../../common/constants';
import getToken from './../../utils/get-token';

/**
 * User add/remove friend action component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @param {string}
 * @param {object}
 * @returns {Component}
 */

const UserAction = ({ id, friend_id, options }) => {
  const [isFriend, setIsFriend] = useState(false);
  const location = useLocation().pathname;

  useEffect(() => {
    fetch(`${BASE_URL}/users/${id}/friends`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.log(resData.msg);
        }

        return setIsFriend(
          resData.data.results.some(user => user.user_id === friend_id)
        );
      })
      .catch(err => console.log(err));
  }, [friend_id]);

  const addFriend = () => {
    fetch(`${BASE_URL}/users/${id}/friends`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        friend_id
      })
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.log(resData.msg);
        }

        return setIsFriend(
          resData.data.results.some(user => user.user_2_id === friend_id)
        );
      })
      .catch(err => console.log(err));
  };

  const removeFriend = () => {
    fetch(`${BASE_URL}/users/${id}/friends`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        friend_id
      })
    })
      .then(res => res.json())
      .then(_ => {
        // console.log(_.msg);
        if (location !== '/bans') {
          options.rerender(users =>
            users.filter(user => user.user_id !== friend_id)
          );
        }

        return setIsFriend(false);
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      {isFriend ? (
        <button className="profile-stats__remove-friend" onClick={removeFriend}>
          Remove Friend
        </button>
      ) : (
        <button className="profile-stats__add-friend" onClick={addFriend}>
          Add Friend
        </button>
      )}
    </>
  );
};

export default UserAction;
