import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../../Context/AuthContext';

/**
 * Header logged user bar component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const HeaderLoggedUser = () => {
  const [options, showOptions] = useState(false);
  const { accountState, setAccountState } = useContext(AuthContext);

  const logOut = () => {
    localStorage.removeItem('token');
    sessionStorage.removeItem('token');

    return setAccountState(null);
  };

  return (
    <div
      className="main-header__logged-screen"
      onClick={() => showOptions(!options)}
    >
      <div className="main-header__logged-screen__user">{`@${accountState.username}`}</div>
      <div className="main-header__logged-screen__avatar">
        {accountState.username[0].toUpperCase()}
      </div>
      <ul
        className={
          options ? 'main-header__options active' : 'main-header__options'
        }
      >
        <li className="main-header__options__item">
          <Link to="/posts" className="main-header__options__link">
            <i className="bi bi-chat-quote"></i>Posts
          </Link>
        </li>
        <li className="main-header__options__item">
          <Link to="/comments" className="main-header__options__link">
            <i className="bi bi-chat-text"></i>Comments
          </Link>
        </li>
        <li className="main-header__options__item">
          <Link to="/friends" className="main-header__options__link">
            <i className="bi bi-people"></i>Friends
          </Link>
        </li>
        <li className="main-header__options__item">
          <Link to="/settings" className="main-header__options__link">
            <i className="bi bi-gear"></i>Settings
          </Link>
        </li>
        <li className="main-header__options__item">
          <Link to="/" className="main-header__options__link" onClick={logOut}>
            <i className="bi bi-box-arrow-right"></i>Log out
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default HeaderLoggedUser;
