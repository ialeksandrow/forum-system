import './Login.css';
import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from './../../Context/AuthContext';
import { BASE_URL } from './../../common/constants';
import decodeToken from './../../utils/decode-token';

/**
 * Login user page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {history}
 * @returns {Component}
 */

const Login = ({ history }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(true);
  const [error, setError] = useState('');
  const { setAccountState } = useContext(AuthContext);

  const userLogin = e => {
    e.preventDefault();
    setError('');

    if (username.trim().length < 5 || username.trim().length > 15) {
      return setError('Username must be between 5 - 15 characters long.');
    }

    if (
      password.trim().length < 5 ||
      password.trim().length > 255 ||
      !password.match(/[A-Z]/) ||
      !password.match(/[0-9]/)
    ) {
      return setError(
        'Password must be more than 5 characters long and must contain at least one uppercase letter and non-alphabetic 0 - 9 character.'
      );
    }

    fetch(`${BASE_URL}/sign-in`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password
      })
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return setError(resData.msg);
        }

        setAccountState(decodeToken(resData.token));

        if (!rememberMe) {
          sessionStorage.setItem('token', resData.token);

          return history.push('/latest');
        }

        localStorage.setItem('token', resData.token);

        return history.push('/latest');
      })
      .catch(_ => setError('Something went wrong.'));
  };

  return (
    <form className="home-login-form" onSubmit={userLogin}>
      <div className="mb-3">
        <label htmlFor="username" className="form-label">
          Username
        </label>
        <input
          type="text"
          name="username"
          id="username"
          className="form-control"
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="password" className="form-label">
          Password
        </label>
        <input
          type="password"
          name="password"
          id="password"
          className="form-control"
          aria-describedby="passwordHelp"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <div id="passwordHelp" className="form-text">
          We'll never share your password with anyone else.
        </div>
      </div>
      <div className="form-check mb-3">
        <input
          type="checkbox"
          name="rememberMe"
          id="rememberMe"
          className="form-check-input"
          checked={rememberMe}
          onChange={e => setRememberMe(e.target.checked)}
        />
        <label htmlFor="rememberMe" className="form-check-label">
          Remember Me
        </label>
      </div>
      <button type="submit" className="btn btn-primary mb-3">
        Log In
      </button>
      <p>
        Don't have an account? <Link to="/register">Sign Up</Link>
      </p>
      {error && <p className="text-danger">{error}</p>}
    </form>
  );
};

export default Login;
