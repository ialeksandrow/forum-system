import './Register.css';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { BASE_URL } from './../../common/constants';

/**
 * Register user page component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {history}
 * @returns {Component}
 */

const Register = ({ history }) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [birthdate, setBirthdate] = useState('');
  const [error, setError] = useState('');
  const [alert, setAlert] = useState(false);

  const userRegister = e => {
    e.preventDefault();
    setError('');

    if (username.trim().length < 5 || username.trim().length > 15) {
      return setError('Username must be between 5 - 15 characters long.');
    }

    if (
      password.trim().length < 5 ||
      password.trim().length > 255 ||
      !password.match(/[A-Z]/) ||
      !password.match(/[0-9]/)
    ) {
      return setError(
        'Password must be more than 5 characters long and must contain at least one uppercase letter and non-alphabetic 0 - 9 character.'
      );
    }

    if (
      email.trim().length < 5 ||
      email.trim().length > 255 ||
      !/([\w\d._]+)@([\w.]+)/.test(email)
    ) {
      return setError('Invalid email address.');
    }

    if (isNaN(Date.parse(birthdate)) || Date.parse(birthdate) > Date.now()) {
      return setError('Invalid date of birth.');
    }

    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password,
        email,
        birthdate
      })
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return setError(resData.msg);
        }

        setAlert(true);

        return setTimeout(() => history.push('/'), 5000);
      })
      .catch(_ => setError('Something went wrong.'));
  };

  return (
    <>
      {alert && (
        <div className="alert alert-success" role="alert">
          <strong>Holy guacamole!</strong> Account successfully created. You
          will be redirected to the login page in 5 seconds, please click{' '}
          <Link to="/" className="alert-link">
            here
          </Link>{' '}
          if your browser does not redirect you automatically.
        </div>
      )}
      <form className="home-register-form" onSubmit={userRegister}>
        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username
          </label>
          <div className="input-group">
            <span className="input-group-text">@</span>
            <input
              type="text"
              name="username"
              id="username"
              className="form-control"
              aria-describedby="usernameHelp"
              value={username}
              onChange={e => setUsername(e.target.value)}
            />
          </div>
          <div id="usernameHelp" className="form-text">
            You can't change username later. Choose wisely.
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password
          </label>
          <input
            type="password"
            name="password"
            id="password"
            className="form-control"
            aria-describedby="passwordHelp"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <div id="passwordHelp" className="form-text">
            We'll never share your password with anyone else.
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            type="email"
            name="email"
            id="email"
            className="form-control"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="birthdate" className="form-label">
            Birthdate
          </label>
          <input
            type="date"
            name="birthdate"
            id="birthdate"
            className="form-control"
            value={birthdate}
            onChange={e => setBirthdate(e.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-success mb-3">
          Sign Up
        </button>
        <p>
          Already have an account? <Link to="/">Sign In</Link>
        </p>
        {error && <p className="text-danger">{error}</p>}
      </form>
    </>
  );
};

export default Register;
