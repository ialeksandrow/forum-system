import './Pagination.css';
import createPages from './../../utils/create-pages';

/**
 * Pagination component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number}
 * @param {number}
 * @param {number}
 * @param {function}
 * @returns {Component}
 */

const Pagination = ({
  dataLength,
  dataPerPage,
  currentPage,
  setCurrentPage
}) => {
  const pages = createPages(Math.ceil(dataLength / dataPerPage));

  return (
    <div className="pagination-menu">
      {currentPage > 1 && (
        <button
          type="button"
          className="pagination-menu__page"
          onClick={() => setCurrentPage(--currentPage)}
        >
          <i className="bi bi-caret-left"></i>
        </button>
      )}
      {pages.map(page => (
        <button
          type="button"
          className={
            page === currentPage
              ? 'pagination-menu__page active'
              : 'pagination-menu__page'
          }
          key={page}
          onClick={() => setCurrentPage(page)}
        >
          {page}
        </button>
      ))}
      {currentPage < Math.ceil(dataLength / dataPerPage) && (
        <button
          type="button"
          className="pagination-menu__page"
          onClick={() => setCurrentPage(++currentPage)}
        >
          <i className="bi bi-caret-right"></i>
        </button>
      )}
    </div>
  );
};

export default Pagination;
