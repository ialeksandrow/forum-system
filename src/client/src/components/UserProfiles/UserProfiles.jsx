import { useContext, useState, useEffect } from 'react';
import AuthContext from './../../Context/AuthContext';
import { BASE_URL } from './../../common/constants';
import getToken from './../../utils/get-token';
import ProfileStats from './../ProfileStats/ProfileStats';
import Pagination from './../Pagination/Pagination';

/**
 * List of users profiles component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string}
 * @param {string}
 * @param {string}
 * @returns {Component}
 */

const UserProfiles = ({ title, endpoint, newBan }) => {
  const { accountState } = useContext(AuthContext);
  const [users, setUsers] = useState(null);
  const [usersCount, setUsersCount] = useState(0);
  const [usersPerPage, setUsersPerPage] = useState(6);
  const [currentPage, setCurrentPage] = useState(1);

  const lastUserIndex = currentPage * usersPerPage;
  const firstUserIndex = lastUserIndex - usersPerPage;

  useEffect(() => {
    fetch(
      `${BASE_URL}/users/${
        endpoint !== 'banned' ? `${accountState.user_id}/` : ''
      }${endpoint}`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    )
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.log(resData.msg);
        }

        setUsersCount(resData.data.results.length);

        return setUsers(
          resData.data.results.slice(firstUserIndex, lastUserIndex)
        );
      })
      .catch(err => console.log(err));
  }, [usersPerPage, currentPage, newBan]);

  if (users && users.length === 0) {
    return (
      <div className="row mt-5">
        <div className="col">
          <div className="text-secondary">
            No users found matching the criteria.
          </div>
        </div>
      </div>
    );
  }

  return (
    <>
      <div className="row mt-5">
        <div className="col-md-8">
          <p className="fs-3 mb-3">{title}</p>
        </div>
        <div className="col-md-4">
          <label htmlFor="range" className="form-label">
            Users Per Page
          </label>
          <input
            type="range"
            min="6"
            max="12"
            step="3"
            className="form-range"
            id="range"
            value={usersPerPage}
            onChange={e => {
              setUsersPerPage(parseInt(e.target.value));
            }}
          />
        </div>
      </div>
      {users ? (
        <div className="row g-5 mt-1">
          {users.map(user => {
            return (
              <div key={user.user_id} className="col-lg-4 col-md-6">
                <ProfileStats
                  id={user.user_id}
                  options={{
                    rerender: setUsers
                  }}
                />
              </div>
            );
          })}
          <Pagination
            dataLength={usersCount}
            dataPerPage={usersPerPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
      ) : null}
    </>
  );
};

export default UserProfiles;
