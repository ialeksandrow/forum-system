import ProfileStats from './ProfileStats';
import './ProfileStatsModal.css';

const ProfileStatsModal = ({ user_id }) => {
  return (
    <>
      <div
        className='modal fade'
        id={'profile-stats-modal-user' + user_id}
        tabIndex='-1'
        aria-labelledby={'profile-stats-modal-user' + user_id}
        aria-hidden='true'
      >
        <div
          className='modal-dialog modal-fullscreen-sm-down p-0'
          style={{ backgroundColor: 'white' }}
        >
          <div className='modal-content profile-stats-modal'>
            <div className='rounded' style={{ backgroundColor: 'white' }}>
              <ProfileStats id={user_id} />
            </div>
            <button
              type='button'
              className='btn-close'
              data-bs-dismiss='modal'
              aria-label='Close'
            ></button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileStatsModal;
