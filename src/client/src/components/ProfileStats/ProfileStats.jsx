import './ProfileStats.css';
import { useState, useEffect, useContext } from 'react';
import AuthContext from './../../Context/AuthContext';
import { BASE_URL } from './../../common/constants';
import getToken from './../../utils/get-token';
import UserAction from './../UserAction/UserAction';

/**
 * Personal user's profile statistics component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} id
 * @param {object} options optional
 * @returns {Component}
 */

const ProfileStats = ({ id, options }) => {
  const { accountState } = useContext(AuthContext);
  const [stats, setStats] = useState(null);
  const [user, setUser] = useState(null);

  useEffect(() => {
    setUser(accountState);
  }, [accountState]);

  useEffect(() => {
    fetch(
      `${BASE_URL}/users/${
        id !== accountState.user_id ? id : accountState.user_id
      }/stats`,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    )
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.log(resData.msg);
        }

        return resData.data;
      })
      .then(userData => setStats(userData))
      .catch(_ => console.log('Something went wrong.'));

    if (id !== accountState.user_id) {
      fetch(`${BASE_URL}/users/${id}`, {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      })
        .then(res => res.json())
        .then(resData => resData.data.results)
        .then(userData => {
          if (userData.msg) {
            return console.log(userData.msg);
          }

          return setUser(userData);
        })
        .catch(_ => console.log('Something went wrong.'));
    }
  }, [id]);

  return (
    <>
      {stats && user ? (
        <div className="profile-stats">
          <div className="profile-stats__avatar">
            {user.username[0].toUpperCase()}
          </div>
          <div className="profile-stats__user-stats">
            <div className="profile-stats__stat">
              <span>{stats.posts_count}</span>
              <span>Posts</span>
            </div>
            <div className="profile-stats__stat">
              <span>{stats.comments_count}</span>
              <span>Comments</span>
            </div>
            <div className="profile-stats__stat">
              <span>{stats.friends_count}</span>
              <span>Friends</span>
            </div>
          </div>
          <div className="profile-stats__user-details">
            {user.first_name && user.last_name
              ? `${user.first_name} ${user.last_name}`
              : `@${user.username}`}
            , 22
          </div>
          <div
            className={
              user.role === 'admin'
                ? 'profile-stats__user-role--admin'
                : 'profile-stats__user-role'
            }
          >
            {user.role.toUpperCase()}
          </div>
          <div className="profile-stats__membership">
            Forum member since
            {` ${new Date(user.registration_date).toLocaleDateString('en-GB')}`}
          </div>
          {id !== accountState.user_id && (
            <UserAction
              id={accountState.user_id}
              friend_id={id}
              options={options}
            />
          )}
        </div>
      ) : (
        <div className="spinner-border text-primary" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      )}
    </>
  );
};

export default ProfileStats;
