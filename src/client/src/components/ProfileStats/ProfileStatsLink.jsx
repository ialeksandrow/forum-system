import ProfileStatsModal from './ProfileStatsModal';

const ProfileStatsLink = ({ user_id, username }) => {
  return (
    <>
      <a
        type="button"
        className="lead"
        data-bs-toggle="modal"
        href={'#profile-stats-modal-user' + user_id}
      >
        @{username}
      </a>
      <ProfileStatsModal user_id={+user_id} />
    </>
  );
};

export default ProfileStatsLink;
