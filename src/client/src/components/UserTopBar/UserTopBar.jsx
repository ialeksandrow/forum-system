import { useContext } from 'react';
import AuthContext from './../../Context/AuthContext';
import Navigation from './../Navigation/Navigation';
import NavigationLink from './../Navigation/NavigationLink';
import { USER_ROLES } from './../../common/constants';

/**
 * User/Admin welcome bar component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {Component}
 */

const UserTopBar = () => {
  const { accountState } = useContext(AuthContext);

  return (
    <>
      {accountState ? (
        <>
          <div className="row">
            <div className="col-6">
              <h1 className="mb-3">
                Hello,{' '}
                {accountState.first_name
                  ? accountState.first_name
                  : `@${accountState.username}`}
              </h1>
              {accountState.role === USER_ROLES.admin ? (
                <p className="mb-5">
                  Welcome back... to the office!
                  <br />
                  All your hard work around here doesn’t go unnoticed.
                </p>
              ) : (
                <p className="mb-5">
                  This is your profile page. You can see the posts you've
                  written, check incoming friendship requests, manage personal
                  information and so much more.
                </p>
              )}
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              {accountState.role === USER_ROLES.admin ? (
                <Navigation>
                  <NavigationLink name="Posts" route="/posts" />
                  <NavigationLink name="Comments" route="/comments" />
                  <NavigationLink name="Friends" route="/friends" />
                  <NavigationLink name="Settings" route="/settings" />
                  <NavigationLink
                    name="Account Deletion"
                    route="/delete-account"
                    option="attention"
                  />
                  <NavigationLink name="User Penalties" route="/bans" />
                </Navigation>
              ) : (
                <Navigation>
                  <NavigationLink name="Posts" route="/posts" />
                  <NavigationLink name="Comments" route="/comments" />
                  <NavigationLink name="Friends" route="/friends" />
                  <NavigationLink name="Settings" route="/settings" />
                  <NavigationLink
                    name="Account Deletion"
                    route="/delete-account"
                    option="attention"
                  />
                </Navigation>
              )}
            </div>
          </div>
        </>
      ) : (
        <div className="row">
          <div className="spinner-border text-primary" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      )}
    </>
  );
};

export default UserTopBar;
