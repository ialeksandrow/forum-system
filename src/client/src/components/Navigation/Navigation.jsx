import './Navigation.css';

/**
 * Header navigation component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {children}
 * @returns {Component}
 */

const Navigation = ({ children }) => {
  return (
    <nav className="main-nav">
      <ul className="main-nav__menu">{children}</ul>
    </nav>
  );
};

export default Navigation;
