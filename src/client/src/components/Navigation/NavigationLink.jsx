import { NavLink } from 'react-router-dom';

/**
 * Header navigation link component
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} name
 * @param {string} route
 * @param {string} option
 * @param {string} containerOption
 * @returns {Component}
 */

const NavigationLink = ({ name, route, option = '', containerOption = '' }) => {
  return (
    <li className={`main-nav__menu__item ${containerOption}`}>
      <NavLink
        exact
        to={route}
        className={
          option ? `main-nav__menu__link ${option}` : 'main-nav__menu__link'
        }
      >
        {name}
      </NavLink>
    </li>
  );
};

export default NavigationLink;
