import { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import AuthContext from './Context/AuthContext';
import decodeToken from './utils/decode-token';
import getToken from './utils/get-token';
import GuardedRoute from './providers/GuardedRoute';
import Header from './components/Header/Header';
import Home from './views/Home/Home';
import Latest from './views/Posts/Latest';
import UserPosts from './views/UserPosts/UserPosts';
import UserComments from './views/UserComments/UserComments';
import UserFriends from './views/UserFriends/UserFriends';
import Settings from './views/Settings/Settings';
import AccountDeletion from './views/AccountDeletion/AccountDeletion';
import UserPenalties from './views/UserPenalties/UserPenalties';
import Thread from './views/Posts/Thread';
import ErrorView from './views/ErrorView';

const App = () => {
  const [accountState, setAccountState] = useState(decodeToken(getToken()));

  return (
    <Router>
      <AuthContext.Provider value={{ accountState, setAccountState }}>
        {accountState && <Header />}
        <Switch>
          <Route path="/login" component={Home} />
          <Route path="/register" component={Home} />
          <GuardedRoute path="/latest" component={Latest} />
          <GuardedRoute path="/posts/:id" component={Thread} />
          <GuardedRoute path="/posts" component={UserPosts} />
          <GuardedRoute path="/comments" component={UserComments} />
          <GuardedRoute path="/friends" component={UserFriends} />
          <GuardedRoute path="/settings" component={Settings} />
          <GuardedRoute path="/delete-account" component={AccountDeletion} />
          <GuardedRoute path="/bans" component={UserPenalties} />
          <Route path="/error" component={ErrorView} />
          <Redirect from="/" to="/login" />
        </Switch>
      </AuthContext.Provider>
    </Router>
  );
};

export default App;
