import { createContext } from 'react';

export const SettingsContext = createContext({
  countPosts: 30,
  setCountPosts: () => {}
});
