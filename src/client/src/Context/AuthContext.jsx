import { createContext } from 'react';

const AuthContext = createContext({
  accountState: null,
  setAccountState: () => {}
});

export default AuthContext;
