import { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthContext from './../Context/AuthContext';

/**
 * Guards unauthenticated users to access application routes
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {Component}
 * @param {any}
 * @returns {Route}
 */

const GuardedRoute = ({ component: Component, ...rest }) => {
  const { accountState } = useContext(AuthContext);

  return (
    <Route
      {...rest}
      render={props =>
        accountState ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
};

export default GuardedRoute;
