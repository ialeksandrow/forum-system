export const BASE_URL = 'http://localhost:5000/api';
export const DATE_LOCALE = 'fr-FR';
export const HOME_ROUTES = {
  login: '/login',
  register: '/register'
};
export const USER_ROLES = {
  user: 'user',
  admin: 'admin'
};
export const BAN_DURATIONS = ['hour', 'day', 'week'];
