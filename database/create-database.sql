DROP DATABASE IF EXISTS forum;

CREATE DATABASE forum;

USE forum;

CREATE TABLE roles (
	role_id TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
	role_type VARCHAR(15) NOT NULL UNIQUE,
	PRIMARY KEY (role_id)
);

CREATE TABLE users (
	user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	username NVARCHAR(15) NOT NULL UNIQUE,
	`password` VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL UNIQUE,
	first_name VARCHAR(30),
	last_name VARCHAR(30),
	birthdate DATE NOT NULL,
	registration_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ban_date TIMESTAMP,
	ban_expiration TIMESTAMP,
	is_banned TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	is_deactivated TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	role_id TINYINT(1) UNSIGNED NOT NULL,
	PRIMARY KEY (user_id),
	FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

CREATE TABLE friends (
	user_1_id INT UNSIGNED NOT NULL,
	user_2_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (user_1_id, user_2_id),
	FOREIGN KEY (user_1_id) REFERENCES users(user_id),
	FOREIGN KEY (user_2_id) REFERENCES users(user_id)
);

CREATE TABLE posts (
	post_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	post_title NVARCHAR(255) NOT NULL,
	post_content LONGTEXT NOT NULL UNIQUE,
	post_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	post_views INT UNSIGNED NOT NULL DEFAULT 0,
	is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	post_history LONGTEXT DEFAULT "[]",
	user_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (post_id),
	FOREIGN KEY (user_id) REFERENCES users(user_id) 
);

CREATE TABLE comments (
	comment_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	comment_content LONGTEXT NOT NULL,
	comment_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	post_id INT UNSIGNED NOT NULL,
	is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	comment_history LONGTEXT DEFAULT "[]",
	user_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (comment_id),
	FOREIGN KEY (user_id) REFERENCES users(user_id),
	FOREIGN KEY (post_id) REFERENCES posts(post_id)
);

CREATE TABLE post_likes (
	user_id INT UNSIGNED NOT NULL,
	post_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (user_id, post_id),
	FOREIGN KEY (user_id) REFERENCES users(user_id),
	FOREIGN KEY (post_id) REFERENCES posts(post_id)
);

CREATE TABLE comment_likes (
	user_id INT UNSIGNED NOT NULL,
	comment_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (user_id, comment_id),
	FOREIGN KEY (user_id) REFERENCES users(user_id),
	FOREIGN KEY (comment_id) REFERENCES comments(comment_id)
);
