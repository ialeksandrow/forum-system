USE forum;

# admin_demo
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Built-In Touch Control Induction Cooktop With 5 Elements', 16, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('I fell in love with this dress but when i received it in the mail, i was really surprised to see that unlike the picture online, the slip underneath is a nude color. it fit well and i decided to keep it. i wore it for the first time today and felt great in it. i was driving with my son in the back seat, i turned to hand him something and the entire right side of the dress (where the crocheting is) ripped open!! i cannot believe how poor the quality is at this price!', 22, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('I love this dress. i am an hourglass and found this dress to be super flattering and such a unique find. i usually avoid pockets in front like this, but it works on this dress. so happy i found it.', 18, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Super cute but big', 19, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('I normally wear a medium with these types of fabrics but this one was oddly proportioned on me. the skirt was also not very flattering to my hourglass figure, but it at least fit well. the top was baggy and gaped at the armpits as stated by others. i really wanted to like this dress but it made me look 20 pounds heavier even in the places it looked too big on me. i could have tried going with a small but the skirt would have then been too short.', 17, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Love this beautiful stylish sweater wrap', 20, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Darling!', 16, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Such a disappointment!', 19, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Fantastic dress', 17, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('10++++++', 22, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Love this beautiful stylish sweater wrap', 18, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Wrinkly cotton, nice shape', 21, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Perfection!', 22, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Uhh Yeah Dude..', 22, 1);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('100% recommended!', 18, 1);
# user_demo
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Desert Island Discs was created by Roy Plomley in 1942, and the format is simple: a guest is invited by Kirsty Young to choose the eight records they would take with them to a desert island', 1, 2);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Each week Nic & The Captain get in the garage and talk true crime and drink beer.', 14, 2);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Nerdy nerdness comedy podcast w/ Chris Hardwick, Jonah Ray & Matt Mira.', 7, 2);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('A weekly round up of America through the eyes of two American Americans', 4, 2);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Ready yourself for a murder adventure hosted by Karen Kilgariff and Georgia Hardstark, two lifelong fans of true crime stories. Each episode the girls tell each other their favorite tales of murder, and hear hometown crime stories from friends and fans. Check your anxiety at the door, cause Karen & Georgia are dying to discuss death.', 11, 2);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('You Must Remember This', 9, 2);
INSERT INTO comments (comment_content, post_id, user_id) VALUES ('Extraordinary stories of ordinary life.', 12, 2);